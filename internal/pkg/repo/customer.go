package repo

import (
	"strconv"
	"time"

	"kyc-service/internal/pkg/repo/models"
	"kyc-service/internal/pkg/repo/utils"

	"github.com/jinzhu/gorm"
)

// CustomerRepository is the interface that implements all Customer methods
type CustomerRepository interface {
	CreateNewCustomer(*models.Customer) error
	SetCustomerKycID(kycID int, customer *models.Customer) error
	GetCustomerRecordsCount() int64
	GetCustomerByID(string) (*models.Customer, error)
	GetCustomerKycIDByID(string) (string, error)
	GetCustomersByBtcs(state models.State, inWorking models.InWork) ([]*models.Customer, error)
	GetCustomersByBtcsImages(state models.State, inWorking models.InWork, status models.StatusImages) ([]*models.Customer, error)
	GetCustomersByNotVerification(imageStatus models.StatusImages, isWebHook models.WebHook, isCronCheckScans models.CheckScans) ([]*models.Customer, error)
	SetImageFlag(flag bool, mediaType models.MediaType, customer *models.Customer) error
	SetFaceImageFlag(bool, *models.Customer) error
	SetBackImageFlag(bool, *models.Customer) error
	SetFrontImageFlag(bool, *models.Customer) error
	SetAllImagesFlags(bool, *models.Customer) error
	SetFaceFormat(format string, customer *models.Customer) error
	SetFrontFormat(format string, customer *models.Customer) error
	SetBackFormat(format string, customer *models.Customer) error
	GetCustomerByKycID(string) (*models.Customer, error)
	SetScansCheck(status string, customer *models.Customer) error
	SetScansCheckResultReceived(flag bool, customer *models.Customer) error
	SetScansCheckResultReceivedAtNow(customer *models.Customer) error
	SetScansCheckResult(result string, customer *models.Customer) error
	SetScansCheckResultSubmitedAt(customer *models.Customer) error
	SetAddressCheckResultReceived(flag bool, customer *models.Customer) error
	SetAddressCheckResultReceivedAtNow(customer *models.Customer) error
	SetAddressCheckResult(result string, customer *models.Customer) error
	DoesAllImageFlagsAreSet(customer *models.Customer) bool
	SaveCustomer(customer *models.Customer) error
	SetCustomerInWork(inWork models.InWork, customer *models.Customer) error
	SetCustomerStatusImages(status models.StatusImages, customer *models.Customer) error
	SetCustomerIsCronCheckScans(webhook models.WebHook, customer *models.Customer) error
	SetCustomerIsWebHook(webhook models.CheckScans, customer *models.Customer) error
	UpdateCustomer(customer *models.Customer, fields map[string]interface{}) error
}

// CustomerRepo contains Customer table methods
type CustomerRepo struct {
	db *gorm.DB
}

// CreateNewCustomer creates customer in DB using Customer models instance
func (repo *CustomerRepo) CreateNewCustomer(customer *models.Customer) error {
	err := repo.db.Create(customer).Error
	if err != nil {
		switch {
		case utils.IsUniqueConstraintError(err):
			return &utils.DuplicateConstraintError{Table: "customer", Field: "id"}
		case utils.IsNotNullConstraintError(err):
			return &utils.NotNullConstrainError{Table: "customer", Field: "id"}
		default:
			return err
		}
	}
	return nil
}

// SetCustomerKycID sets KycID field of provided customer with provided int
func (repo *CustomerRepo) SetCustomerKycID(kycID int, customer *models.Customer) error {
	customer.KycID = strconv.Itoa(kycID)
	err := repo.db.Save(customer).Error
	if err != nil {
		return err
	}

	return nil
}

// Steps BTCS
func (repo *CustomerRepo) GetCustomersByNotVerification(imageStatus models.StatusImages, isWebHook models.WebHook, isCronCheckScans models.CheckScans) ([]*models.Customer, error) {
	var customers []*models.Customer

	err := repo.db.Where(
		"kyc_id != '' AND scans_check_result_received = false AND scans_check_result_received_at IS NULL AND status_btcs_images = ? AND is_web_hook = ? AND is_cron_check_scans = ? AND created_at <= NOW() - interval '15 minutes'",
		imageStatus,
		isWebHook,
		isCronCheckScans).Limit(100).Find(&customers).Error
	if err != nil {
		return customers, err
	}

	return customers, nil
}

func (repo *CustomerRepo) GetCustomersByBtcs(state models.State, inWorking models.InWork) ([]*models.Customer, error) {
	var customers []*models.Customer

	err := repo.db.Where("state = ? AND in_work = ?", state, inWorking).Limit(100).Find(&customers).Error
	if err != nil {
		return customers, err
	}

	return customers, nil
}

func (repo *CustomerRepo) GetCustomersByBtcsImages(state models.State, inWorking models.InWork, status models.StatusImages) ([]*models.Customer, error) {
	var customers []*models.Customer

	err := repo.db.Where("state = ? AND in_work = ? AND status_btcs_images = ?", state, inWorking, status).Limit(100).Find(&customers).Error
	if err != nil {
		return customers, err
	}

	return customers, nil
}

func (repo *CustomerRepo) SetCustomerIsCronCheckScans(webhook models.CheckScans, customer *models.Customer) error {
	err := repo.db.Model(&customer).UpdateColumn("is_cron_check_scans", webhook).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *CustomerRepo) SetCustomerIsWebHook(webhook models.WebHook, customer *models.Customer) error {
	err := repo.db.Model(&customer).UpdateColumn("is_web_hook", webhook).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *CustomerRepo) SetCustomerInWork(inWork models.InWork, customer *models.Customer) error {
	err := repo.db.Model(&customer).UpdateColumn("in_work", inWork).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *CustomerRepo) SetCustomerStatusImages(status models.StatusImages, customer *models.Customer) error {
	err := repo.db.Model(&customer).UpdateColumn("status_btcs_images", status).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *CustomerRepo) UpdateCustomer(customer *models.Customer, fields map[string]interface{}) error {
	err := repo.db.Model(&customer).Updates(fields).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *CustomerRepo) SaveCustomer(customer *models.Customer) error {
	err := repo.db.Save(customer).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *CustomerRepo) GetCustomerRecordsCount() int64 {
	var count int64
	repo.db.Model(&models.Customer{}).Count(&count)
	return count
}

// GetCustomerByID returns &customer instance that searched in DB by ID key
func (repo *CustomerRepo) GetCustomerByID(id string) (*models.Customer, error) {

	var customer models.Customer

	repo.db.Where("id = ?", id).Find(&customer)

	if customer.ID == "" {
		return nil, &utils.DoesNotExistsError{Table: "customer", Field: "id"}
	}

	return &customer, nil
}

// GetCustomerByKycID returns &customer instance that searchd in DB by ID key
func (repo *CustomerRepo) GetCustomerByKycID(id string) (*models.Customer, error) {

	var customer models.Customer

	repo.db.Where("kyc_id = ?", id).Find(&customer)

	if customer.ID == "" {
		return nil, &utils.DoesNotExistsError{Table: "customer", Field: "kyc_id"}
	}

	return &customer, nil
}

// GetCustomerKycIDByID returns &customer instance KYCID field that searchd in DB by ID key
func (repo *CustomerRepo) GetCustomerKycIDByID(id string) (string, error) {
	customer, err := repo.GetCustomerByID(id)
	if err != nil {
		return "", err
	}

	return customer.KycID, nil
}

// SetImageFlag updates upload flag field for given media type
func (repo *CustomerRepo) SetImageFlag(flag bool, mediaType models.MediaType, customer *models.Customer) error {
	flagField, err := customer.GetMediaTypeDBFlagFieldName(mediaType)
	if err != nil {
		return err
	}

	err = repo.db.Model(customer).Select(flagField).Updates(map[string]interface{}{flagField: flag}).Error
	if err != nil {
		return err
	}

	return nil
}

// SetFaceImageFlag sets FaceUploaded field of provided customer with provided bool
func (repo *CustomerRepo) SetFaceImageFlag(flag bool, customer *models.Customer) error {
	customer.FaceUploaded = flag
	err := repo.db.Save(customer).Error
	if err != nil {
		return err
	}

	return nil
}

// SetBackImageFlag sets BackUploaded field of provided customer with provided bool
func (repo *CustomerRepo) SetBackImageFlag(flag bool, customer *models.Customer) error {
	customer.BackUploaded = flag
	err := repo.db.Save(customer).Error
	if err != nil {
		return err
	}
	return nil
}

// SetFrontImageFlag sets FrontUploaded field of provided customer with provided bool
func (repo *CustomerRepo) SetFrontImageFlag(flag bool, customer *models.Customer) error {
	customer.FrontUploaded = flag
	err := repo.db.Save(customer).Error
	if err != nil {
		return err
	}
	return nil
}

// SetFaceFormat sets FaceFormat field with provided format
func (repo *CustomerRepo) SetFaceFormat(format string, customer *models.Customer) error {
	err := repo.db.Model(customer).Update("face_format", format).Error
	if err != nil {
		return err
	}
	return nil
}

// SetBackFormat sets BackFormat field with provided format
func (repo *CustomerRepo) SetBackFormat(format string, customer *models.Customer) error {
	err := repo.db.Model(customer).Update("back_format", format).Error
	if err != nil {
		return err
	}
	return nil
}

// SetFrontFormat sets FrontFormat field with provided format
func (repo *CustomerRepo) SetFrontFormat(format string, customer *models.Customer) error {
	err := repo.db.Model(customer).Update("front_format", format).Error
	if err != nil {
		return err
	}
	return nil
}

// SetAllImagesFlags sets all Images flags of provided customer with provided boolean flag
func (repo *CustomerRepo) SetAllImagesFlags(flag bool, customer *models.Customer) error {
	customer.FrontUploaded = flag
	customer.BackUploaded = flag
	customer.FaceUploaded = flag
	repo.db.Save(customer)
	return nil
}

// DoesAllImageFlagsAreSet returns bool that defines does all Images flags are true
func (repo *CustomerRepo) DoesAllImageFlagsAreSet(customer *models.Customer) bool {
	return customer.FrontUploaded && customer.BackUploaded && customer.FaceUploaded
}

// SetScansCheck treansactionally sets all required field when processing Scans verification event
func (repo *CustomerRepo) SetScansCheck(status string, customer *models.Customer) error {
	tx := repo.db.Begin()
	// TODO: looks like it is not happens atomic. Add more tests
	// Settings repo db instans with created atomic transaction
	err := repo.SetScansCheckResultReceived(true, customer)
	err = repo.SetScansCheckResult(status, customer)
	err = repo.SetScansCheckResultReceivedAtNow(customer)
	// if status != "APPROVE" {
	// 	err = repo.SetAllImagesFlags(false, customer)
	// }
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()

	return nil
}

// SetScansCheckResultReceived sets ScansCheckResultReceived of provided customer with provided bool
func (repo *CustomerRepo) SetScansCheckResultReceived(flag bool, customer *models.Customer) error {
	err := repo.db.Model(customer).Update("scans_check_result_received", flag).Error
	if err != nil {
		return err
	}
	return nil
}

// SetScansCheckResultReceivedAtNow sets field ScansCheckResultReceivedAt of provided customer to Time.now
func (repo *CustomerRepo) SetScansCheckResultReceivedAtNow(customer *models.Customer) error {
	err := repo.db.Model(customer).Update("scans_check_result_received_at", time.Now()).Error
	if err != nil {
		return err
	}
	return nil
}

// SetScansCheckResult sets field ScansCheckResult with provided result for provided customer
func (repo *CustomerRepo) SetScansCheckResult(result string, customer *models.Customer) error {
	err := repo.db.Model(customer).Update("scans_check_result", result).Error
	if err != nil {
		return err
	}
	return nil
}

// SetScansCheckResultSubmitedAt sets field ScansCheckResult with current Time for provided customer
func (repo *CustomerRepo) SetScansCheckResultSubmitedAt(customer *models.Customer) error {
	err := repo.db.Model(customer).Update("scans_check_submited_at", time.Now()).Error
	if err != nil {
		return err
	}

	return nil
}

// SetAddressCheckResultReceived sets flag AddressCheckResultReceived to provided flag for provided customer
func (repo *CustomerRepo) SetAddressCheckResultReceived(flag bool, customer *models.Customer) error {
	err := repo.db.Model(customer).Update("address_check_result_received", flag).Error
	if err != nil {
		return err
	}
	return nil
}

// SetAddressCheckResultReceivedAtNow sets field AddressCheckResultReceivedAt of provided customer to Time.now
func (repo *CustomerRepo) SetAddressCheckResultReceivedAtNow(customer *models.Customer) error {
	err := repo.db.Model(customer).Update("address_check_result_received_at", time.Now()).Error
	if err != nil {
		return err
	}
	return nil
}

// SetAddressCheckResult sets field AddressCheckResult with provided result for provided customer
func (repo *CustomerRepo) SetAddressCheckResult(result string, customer *models.Customer) error {
	customer.AddressCheckResult = result

	err := repo.db.Save(customer).Error
	if err != nil {
		return err
	}

	return nil
}

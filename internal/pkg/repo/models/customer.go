package models

import (
	"errors"
	"fmt"
	"github.com/lib/pq"
	"strings"
	"time"

	_ "github.com/lib/pq"
)

type Customer struct {
	ID                           string `gorm:"primary_key"`
	CreatedAt                    time.Time
	UpdatedAt                    time.Time
	KycID                        string `gorm:"unique"`
	FaceUploaded                 bool
	FrontUploaded                bool
	BackUploaded                 bool
	FaceFormat                   string
	FrontFormat                  string
	BackFormat                   string
	SubmitedAt                   *time.Time
	ScansCheckResultReceived     bool
	ScansCheckResultReceivedAt   *time.Time
	ScansCheckResult             string
	ScansCheckSubmitedAt         *time.Time
	AddressCheckResultReceived   bool
	AddressCheckResultReceivedAt *time.Time
	AddressCheckResult           string
	EthAddress                   string         `gorm:"column:eth_address"`
	FirstName                    string         `gorm:"column:first_name"`
	LastName                     string         `gorm:"column:last_name"`
	Email                        string         `gorm:"column:email"`
	Phone                        pq.StringArray `gorm:"type:varchar(22)[];column:phone"`
	Dob                          string         `gorm:"column:dob"`
	State                        int            `gorm:"column:state;default:0"`
	InWork                       int            `gorm:"column:in_work;default:0"`
	StatusImages                 int            `gorm:"column:status_btcs_images;default:0"`
	IsWebHook                    int            `gorm:"column:is_web_hook;default:0"`
	IsCronCheckScans             int            `gorm:"column:is_cron_check_scans;default:0"`
}

func (Customer) IsValidMediaType(token string) bool {
	for _, item := range []MediaType{FrontImage, BackImage, FaceImage} {
		if item == token {
			return true
		}
	}

	return false
}

func (Customer) GetMediaTypeDBFlagFieldName(mediaType MediaType) (string, error) {
	if len(strings.Split(mediaType, "_")) != 2 {
		return "", errors.New("invalid media type")
	}
	mediaTypePrefix := strings.Split(mediaType, "_")[0]
	return fmt.Sprintf("%s_uploaded", mediaTypePrefix), nil
}

type MediaType = string
type State = int
type InWork = int
type StatusImages = int
type WebHook = int
type CheckScans = int

const (
	UniqueConstraintKycID = "customers_kyc_id_key"
	UniqueConstraintID    = "customers_pkey"

	FrontImage MediaType = "front_image"
	BackImage  MediaType = "back_image"
	FaceImage  MediaType = "face_image"

	StateNew            State = 0
	StateCreatedUser    State = 1
	StateSaveEthAddress State = 2
	StateSendImages     State = 3

	WorkingNew          InWork = 0
	WorkingInProcessing InWork = 1

	ImagesNew     StatusImages = 0
	ImagesSuccess StatusImages = 1
	ImagesFailed  StatusImages = 2

	WebHookNew  WebHook = 0
	WebHookDone WebHook = 1

	CheckScansNew  CheckScans = 0
	CheckScansDone CheckScans = 1
)

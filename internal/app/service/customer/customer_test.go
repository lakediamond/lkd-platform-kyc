package customer_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	_ "reflect"
	"strconv"
	"testing"

	"kyc-service/internal/app/service/customer"
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo"
	repo_utils "kyc-service/internal/pkg/repo/utils"
	"kyc-service/internal/pkg/testutils"
	btcsclient "kyc-service/pkg/btcsclient"

	"github.com/stretchr/testify/suite"
)

// TODO: rewrite mock fabric
// func NewMockedBTCSClient() btcsclient.BTCSClient {
// 	return new(btcsclient.MockedBTCSClient)
// }

type CustomerTestSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
}

func TestCustomerTestSuite(t *testing.T) {
	suite.Run(t, new(CustomerTestSuite))
}

// SetupTest will be called before each test
func (suite *CustomerTestSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbClient()
	suite.App = app
	suite.MockServer = mockServer
	err := testutils.UploadFixtures(app, "customer_fixtures.json")
	if err != nil {
		panic(err)
	}
}

// TearDownTest will be called after each test
func (suite *CustomerTestSuite) TearDownTest() {
	// TODO: replace with DB transactions
	suite.App.Repo.DB.Exec("DELETE FROM customers;")
}

func (suite *CustomerTestSuite) TestCreateCustomer() {

	response := btcsclient.GetCustomerResponse{
		FirstName:      "Kirill",
		LastName:       "Pisariev",
		CandidateID:    "5c016e33346265000300231d",
		DateCreated:    "2018-11-30 17:06:57.701122",
		DateUpdated:    "2018-12-04 14:29:12.766103",
		ID:             5724044800294912,
		PrimaryContact: "+63916123",
		Phone:          []string{"+63916123"},
	}

	mocks := make(map[string]interface{})
	mocks["CreateCustomer"] = &response
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}

	request := &customer.CreateCustomerData{
		FirstName:  "Kirill",
		LastName:   "Pisariev",
		Email:      "k.pisarev+1@edenlab.com.ua",
		Phone:      []string{"+380666776780"},
		Dob:        "1985-04-12",
		EthAddress: "k.pisarev+1@edenlab.com.ua",
		ID:         "12313sadasd1",
	}

	tmp, _ := json.Marshal(request)
	requestData := bytes.NewReader(tmp)
	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("create_customer"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusCreated, resp.StatusCode, "Response should be")

	customer, _ := suite.App.Repo.Customer.GetCustomerByID(request.ID)
	suite.Equal(strconv.Itoa(response.ID), customer.KycID)
}
func (suite *CustomerTestSuite) TestCreateCustomerErrorResponseFromBTCS() {

	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: nil, Error: &btcsclient.BtcsApiError{Code: 500}}

	request := &customer.CreateCustomerData{
		FirstName:  "Kirill",
		LastName:   "Pisariev",
		Email:      "k.pisarev+1@edenlab.com.ua",
		Phone:      []string{"+380666776780"},
		Dob:        "1985-04-12",
		EthAddress: "k.pisarev+1@edenlab.com.ua",
		ID:         "12313sadasd1",
	}

	tmp, _ := json.Marshal(request)
	requestData := bytes.NewReader(tmp)
	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("create_customer"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusBadRequest, resp.StatusCode, "Response should be")
}
func (suite *CustomerTestSuite) TestCheckErrorWhenTakingCustomerKycIDbyID() {
	_, err := suite.App.Repo.Customer.GetCustomerKycIDByID("1232312s")

	if suite.Error(err) {
		suite.Equal(&repo_utils.DoesNotExistsError{Table: "customer", Field: "id"}, err, "Error should be DoesNotExists")
	}
	_, ok := err.(*repo_utils.DoesNotExistsError)
	suite.Equal(ok, true, "Error should be DoesNotExists")
}

func (suite *CustomerTestSuite) TestRequestToCreateCustomerWithoutID() {
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{}

	request := &customer.CreateCustomerData{
		FirstName:  "Kirill",
		LastName:   "Pisariev",
		Email:      "k.pisarev+1@edenlab.com.ua",
		Phone:      []string{"+380666776780"},
		Dob:        "1985-04-12",
		EthAddress: "k.pisarev+1@edenlab.com.ua",
	}
	tmp, _ := json.Marshal(request)
	requestData := bytes.NewReader(tmp)
	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("create_customer"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusBadRequest, resp.StatusCode, "Response should be")

	type cError struct {
		Error   string
		Message string
	}

	responseError := new(cError)

	_ = json.NewDecoder(resp.Body).Decode(responseError)

	suite.Equal(responseError.Error, "NotNullConstraint")
}

func (suite *CustomerTestSuite) TestRequestToCreateCustomerDuplicateID() {
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{}

	request := &customer.CreateCustomerData{
		FirstName:  "Kirill",
		LastName:   "Pisariev",
		Email:      "k.pisarev+1@edenlab.com.ua",
		Phone:      []string{"+380666776780"},
		Dob:        "1985-04-12",
		EthAddress: "k.pisarev+1@edenlab.com.ua",
		ID:         "9b131a6a-f81b-4300-b3a3-9e32ad41b9c3",
	}
	tmp, _ := json.Marshal(request)
	requestData := bytes.NewReader(tmp)
	resp, _ := http.Post(suite.MockServer.URL+suite.App.FindRoute("create_customer"), "application/json", requestData)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusBadRequest, resp.StatusCode, "Response should be")

	type cError struct {
		Error   string
		Message string
	}

	responseError := new(cError)

	_ = json.NewDecoder(resp.Body).Decode(responseError)

	suite.Equal(responseError.Error, "DuplicateConstraint")
}
func (suite *CustomerTestSuite) TestRequestToUpdateCustomerSuccess() {
	response := btcsclient.GetCustomerResponse{
		FirstName:      "Kirill",
		LastName:       "Pisariev123",
		CandidateID:    "5c016e33346265000300231d",
		DateCreated:    "2018-11-30 17:06:57.701122",
		DateUpdated:    "2018-12-04 14:29:12.766103",
		ID:             5724044800294912,
		PrimaryContact: "+63916123",
		Phone:          []string{"+63916123"},
	}
	mocks := make(map[string]interface{})
	mocks["GetCustomer"] = &response
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}

	request := &btcsclient.UpdateCustomerRequest{
		FirstName: "Kirill",
		LastName:  "Pisariev123",
	}
	tmp, _ := json.Marshal(request)
	requestData := bytes.NewReader(tmp)

	// TODO: add pattern change for URL parametrs with regexp
	url := suite.MockServer.URL + "/customer/9b131a6a-f81b-4300-b3a3-9e32ad41b9c3"
	req, _ := http.NewRequest("PATCH", url, requestData)
	req.Header.Add("Content-Type", "application/json")
	client := &http.Client{}
	resp, _ := client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusAccepted, resp.StatusCode, "Response should be")

}
func (suite *CustomerTestSuite) TestRequestToUpdateCustomerWrongID() {
	response := btcsclient.GetCustomerResponse{
		FirstName:      "Kirill",
		LastName:       "Pisariev123",
		CandidateID:    "5c016e33346265000300231d",
		DateCreated:    "2018-11-30 17:06:57.701122",
		DateUpdated:    "2018-12-04 14:29:12.766103",
		ID:             5724044800294912,
		PrimaryContact: "+63916123",
		Phone:          []string{"+63916123"},
	}

	mocks := make(map[string]interface{})
	mocks["GetCustomer"] = &response
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}

	request := &btcsclient.UpdateCustomerRequest{
		FirstName: "Kirill",
		LastName:  "Pisariev123",
	}
	tmp, _ := json.Marshal(request)
	requestData := bytes.NewReader(tmp)

	// TODO: add pattern change for URL parametrs with regexp
	url := suite.MockServer.URL + "/customer/123-123-123-123-123"
	req, _ := http.NewRequest("PATCH", url, requestData)
	req.Header.Add("Content-Type", "application/json")
	client := &http.Client{}
	resp, _ := client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(404, resp.StatusCode, "Response should be")

}
func (suite *CustomerTestSuite) TestUpdateCustomerErrorResponseFormBTCS() {

	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{
		Responses: nil,
		Error: &btcsclient.BtcsApiError{
			Code:         500,
			ErrorMessage: "Some Error",
		},
	}

	request := &btcsclient.UpdateCustomerRequest{
		FirstName: "Kirill",
		LastName:  "Pisariev123",
	}
	tmp, _ := json.Marshal(request)
	requestData := bytes.NewReader(tmp)

	// TODO: add pattern change for URL parametrs with regexp
	url := suite.MockServer.URL + "/customer/9b131a6a-f81b-4300-b3a3-9e32ad41b9c3"
	req, _ := http.NewRequest("PATCH", url, requestData)
	req.Header.Add("Content-Type", "application/json")
	client := &http.Client{}
	resp, _ := client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(400, resp.StatusCode, "Response should be")

	type cError struct {
		Error   string
		Message string
	}

	responseError := new(cError)

	_ = json.NewDecoder(resp.Body).Decode(responseError)

	suite.Equal(responseError.Error, "BadResponse")
	suite.Equal(responseError.Message, "Bad Response! Code:500. Message:Some Error")
}

func (suite *CustomerTestSuite) TestGetCusomterLevels() {

	response := btcsclient.CustomerLevels{
		BitmaskLevel: 94804,
		BitmaskStr:   "000000000000000000010111001001010100",
		CheckedFlags: []string{
			"VERIFIED_PHONE_MOBILE",
			"SUBMITTED_NAME",
			"VERIFIED_NAME_SANCTIONS_SCREEN",
			"ACTIVE",
			"SUBMITTED_PHONE",
			"SUBMITTED_EMAIL",
			"SUBMITTED_DOB",
			"SUBMITTED_ZIP",
		},
	}

	mocks := make(map[string]interface{})
	mocks["GetCustomerLevels"] = &response
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{Responses: mocks, Error: nil}

	// TODO: add pattern change for URL parametrs with regexp
	url := suite.MockServer.URL + "/customer/9b131a6a-f81b-4300-b3a3-9e32ad41b9c3"
	resp, _ := http.Get(url)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusOK, resp.StatusCode, "Response should be")
}

func (suite *CustomerTestSuite) TestGetCusomterLevelsBadResponseFromBTCS() {

	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{
		Responses: nil,
		Error: &btcsclient.BtcsApiError{
			Code:         500,
			ErrorMessage: "Some Error",
		},
	}

	// TODO: add pattern change for URL parametrs with regexp
	url := suite.MockServer.URL + "/customer/9b131a6a-f81b-4300-b3a3-9e32ad41b9c3"
	resp, _ := http.Get(url)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusBadRequest, resp.StatusCode, "Response should be")
	type cError struct {
		Error   string
		Message string
	}

	responseError := new(cError)

	_ = json.NewDecoder(resp.Body).Decode(responseError)

	suite.Equal(responseError.Error, "BadResponse")
	suite.Equal(responseError.Message, "Bad Response! Code:500. Message:Some Error")
}

func (suite *CustomerTestSuite) TestGetCusomterLevelsWrongCustomerID() {

	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{}

	// TODO: add pattern change for URL parametrs with regexp
	url := suite.MockServer.URL + "/customer/123"
	resp, _ := http.Get(url)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusNotFound, resp.StatusCode, "Response should be")
	type cError struct {
		Error   string
		Message string
	}

	responseError := new(cError)

	_ = json.NewDecoder(resp.Body).Decode(responseError)

	suite.Equal(responseError.Error, "DoesNotExists")
}

func (suite *CustomerTestSuite) TestSetVerifiedPhoneSmsError() {
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{
		Responses: nil,
		Error: &btcsclient.BtcsApiError{
			Code:         500,
			ErrorMessage: "Some Error",
		},
	}
	// TODO: add pattern change for URL parametrs with regexp
	url := suite.MockServer.URL + "/customer/9b131a6a-f81b-4300-b3a3-9e32ad41b9c3/verify/phone_sms"
	resp, _ := http.Post(url, "application/json", nil)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusBadRequest, resp.StatusCode, "Response should be")
	type cError struct {
		Error   string
		Message string
	}

	responseError := new(cError)

	_ = json.NewDecoder(resp.Body).Decode(responseError)

	suite.Equal(responseError.Error, "BadResponse")
	suite.Equal(responseError.Message, "Bad Response! Code:500. Message:Some Error")
}

func (suite *CustomerTestSuite) TestSetVerifiedPhoneSmsOk() {
	suite.App.BTCSClient = &btcsclient.MockedBTCSClient{}

	// TODO: add pattern change for URL parametrs with regexp
	url := suite.MockServer.URL + "/customer/9b131a6a-f81b-4300-b3a3-9e32ad41b9c3/verify/phone_sms"
	resp, _ := http.Post(url, "application/json", nil)
	if resp != nil {
		defer resp.Body.Close()
	}
	suite.Equal(http.StatusAccepted, resp.StatusCode, "Response should be")
}

// func (suite *CustomerTestSuite) TestUploadFile() {
// 	suite.App.S3Client = utils.NewS3Client()
// 	suite.App.BTCSClient = utils.NewBTCSClient()
// 	err := customer.SubmitCustomerIDScansFromS3Service(suite.App, "60d02b15-cd5f-4cb2-8fbf-acad624e8bb9")
// 	suite.Nil(err)
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// }

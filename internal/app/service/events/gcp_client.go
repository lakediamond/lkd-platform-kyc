package events

import (
	"cloud.google.com/go/pubsub"
	"context"
	"github.com/rs/zerolog/log"
)

func gcpClient(projectID string) *pubsub.Client {
	client, err := pubsub.NewClient(context.Background(), projectID)
	if err != nil {
		log.Fatal().Err(err).Msg("Subscriber init failed")
	}

	return client
}

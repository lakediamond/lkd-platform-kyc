package application

import (
	"context"
	"kyc-service/internal/pkg/repo"
	"kyc-service/internal/pkg/utils"
	"kyc-service/pkg/btcsclient"
	"kyc-service/pkg/env"
	"kyc-service/pkg/lkdbackclient"
	"net/http"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type key int

const (
	ApplicationKey key = 0
)

type Config struct {
	LogLevel                   string
	Port                       string
	ProjectID                  string
	EventSubscriptionTopic     string
	CronEventSubscriptionTopic string
	UsedMimeTypes              string
}

type Application struct {
	Config        Config
	routes        Routes
	Repo          *repo.Repo
	BTCSClient    btcsclient.BTCSClient
	LKDBackClient lkdbackclient.LKDBackClient
	Storage       *utils.Storage
}

type Route struct {
	Alias   string
	Method  string
	Path    string
	Handler http.Handler
}

// Routes represents list of routes
type Routes []Route

func NewKycService() *Application {
	config := compileConfig()
	app := Application{Config: config}
	app.SetLoggerLevel()
	return &app
}

func (app *Application) SetLoggerLevel() {
	level, err := zerolog.ParseLevel(app.Config.LogLevel)
	if err != nil {
		level = zerolog.DebugLevel
	}
	zerolog.SetGlobalLevel(level)
	log.Info().Msgf("Log level set to %s", level.String())
}

func (app *Application) MountRoutes(routes Routes) {
	app.routes = routes
}

func (app *Application) FindRoute(alias string) string {
	var supportedAliases []string
	for _, route := range app.routes {
		supportedAliases = append(supportedAliases, route.Alias)
		if route.Alias == alias {
			return route.Path
		}
	}

	log.Panic().Msgf("route with alias %s not found. Supported aliases: %s", alias, supportedAliases)

	return ""
}

func (app *Application) WithApplicationContext(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), ApplicationKey, app)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

func compileConfig() Config {
	myEnv := env.GetEnv()

	return Config{
		LogLevel:                   myEnv["LOG_LEVEL"],
		Port:                       myEnv["PORT"],
		ProjectID:                  myEnv["GCP_PROJECT_ID"],
		EventSubscriptionTopic:     myEnv["GCP_EVENT_TOPIC"],
		CronEventSubscriptionTopic: myEnv["GCP_CRON_EVENT_KYC_ID_TOPIC"],
		UsedMimeTypes:              myEnv["GCP_USED_MIME_TYPES"],
	}
}

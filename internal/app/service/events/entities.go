package events

import (
	"errors"
	"mime"
	"regexp"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

type GcpImageMessage struct {
	Kind                    string    `json:"kind"`
	ID                      string    `json:"id"`
	SelfLink                string    `json:"selfLink"`
	Name                    string    `json:"name"`
	Bucket                  string    `json:"bucket"`
	Generation              string    `json:"generation"`
	Metageneration          string    `json:"metageneration"`
	ContentType             string    `json:"contentType"`
	TimeCreated             time.Time `json:"timeCreated"`
	Updated                 time.Time `json:"updated"`
	StorageClass            string    `json:"storageClass"`
	TimeStorageClassUpdated time.Time `json:"timeStorageClassUpdated"`
	Size                    string    `json:"size"`
	Md5Hash                 string    `json:"md5Hash"`
	MediaLink               string    `json:"mediaLink"`
	Crc32C                  string    `json:"crc32c"`
	Etag                    string    `json:"etag"`
	EventBasedHold          bool      `json:"eventBasedHold"`
}

func (gcp *GcpImageMessage) IsValidateImageMimeType(UsedMimeTypes string) bool {
	mimeTypes := strings.Split(UsedMimeTypes, ",")
	mediaType, _, err := mime.ParseMediaType(gcp.ContentType)
	if err != nil {
		log.Error().Err(err).Msgf("Not validate mime type")
		return false
	}

	for _, checkMimeType := range mimeTypes {
		if checkMimeType == mediaType {
			return true
		}
	}

	return false
}

func (gcp *GcpImageMessage) IsValidateStorageName() bool {
	data := strings.Split(gcp.Name, "/")
	if len(data) == 3 {
		return true
	}

	return false
}

func (gcp *GcpImageMessage) IsBucket() bool {
	if gcp.Bucket != "" {
		return true
	}

	return false
}

func (gcp *GcpImageMessage) GetStorageCustomerID() (string, error) {
	if !gcp.IsValidateStorageName() {
		return "", errors.New("can not find customerID")
	}

	customerID := strings.Split(gcp.Name, "/")[0]

	return customerID, nil
}

func (gcp *GcpImageMessage) GetStorageUploadedFileName() (string, error) {
	if !gcp.IsValidateStorageName() {
		return "", errors.New("can not find uploadedFile")
	}

	uploadedFile := strings.Split(gcp.Name, "/")[2]
	matched, _ := regexp.MatchString("([a-zA-Z0-9]+)\\.([a-zA-Z0-9]+){3,4}", uploadedFile)
	if !matched {
		return "", errors.New("can not validate fileName")
	}

	return uploadedFile, nil
}

func ParseStorageFileName(fileName string) (string, string, error) {
	tmp := strings.Split(fileName, ".")
	if len(tmp) == 2 {
		return tmp[0], tmp[1], nil
	}

	return "", "", errors.New("can not parsing fileName " + fileName)
}

// BtcsScansEvent struct that represents notification event about precossed scans from BTCS

// BtcsScansEvent struct that represents notification event about precossed scans from BTCS
type BtcsScansEvent struct {
	Status     string `json:"status"`
	CustomerID uint64 `json:"customer_id"`
	Details    string `json:"details"`
}

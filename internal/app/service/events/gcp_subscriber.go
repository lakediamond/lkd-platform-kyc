package events

import (
	"context"
	"encoding/json"
	"kyc-service/internal/pkg/application"
	"strings"

	"kyc-service/internal/pkg/repo/utils"

	"github.com/rs/zerolog/log"

	"cloud.google.com/go/pubsub"
)

func Subscribe(app *application.Application) {
	client := gcpClient(app.Config.ProjectID)
	defer func() {
		err := client.Close()
		if err != nil {
			log.Fatal().Err(err).Msg("Failed to close gcpClient")
		}
	}()

	t := client.Topic(app.Config.EventSubscriptionTopic)
	defer t.Stop()

	sub := client.Subscription(app.Config.EventSubscriptionTopic)
	err := sub.Receive(context.Background(), func(ctx context.Context, m *pubsub.Message) {
		log.Info().Msgf("ImageSubscribe -> message: %+s\n", string(m.Data))

		if m.Attributes["eventType"] == "OBJECT_FINALIZE" {

			message := &GcpImageMessage{}
			err := json.Unmarshal(m.Data, message)
			if err != nil {
				log.Info().Msgf("Invalid message event - %v", m.Data)
				m.Ack()
				return
			}

			if !strings.Contains(m.Attributes["objectId"], "invalid") && message.IsValidateImageMimeType(app.Config.UsedMimeTypes) {
				err := UpdateFileFlagBasedOnEvent(app, message)
				if err != nil {
					log.Error().Err(err).Msg("Error during updating file flag")
					switch err.(type) {
					case *utils.DoesNotExistsError:
						m.Ack()
					default:
						return
					}
					return
				}
			} else {
				log.Info().Msgf("Invalid object event to skip - %v", m.Attributes["objectId"])
				m.Ack()
			}
		}
		m.Ack()
	})
	if err != context.Canceled {
		log.Error().Err(err)
	}
}

package migrations

import (
	"kyc-service/internal/pkg/repo/models"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddScansFormat = gormigrate.Migration{
	ID: "06e9c3b2-245d-469d-b4ab-223cf443c189",
	Migrate: func(tx *gorm.DB) error {
		error := tx.AutoMigrate(&models.Customer{}).Error
		if error != nil {
			return error
		}
		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE customers 
		DROP COLUMN face_format;
		DROP COLUMN back_format;
		DROP COLUMN front_format;`
		return tx.Exec(req).Error
	},
}

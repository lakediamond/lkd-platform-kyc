package events

import (
	"cloud.google.com/go/pubsub"
	"context"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo/models"
	"kyc-service/pkg/btcsclient"
	"kyc-service/pkg/env"
	"os"
	"strconv"

	"gitlab.com/lakediamond/lkd-pubsub-wrapper"
	"gitlab.com/lakediamond/lkd-pubsub-wrapper/pools"
	customer_service "kyc-service/internal/app/service/customer"
)

const (
	CreateCustomer            string = "kycCreateBtcsCustomer"
	SendEthAddress            string = "kycBtcsEthAddressScreening"
	UploadImagesScans         string = "kycBtcsUploadIDScans"
	VerificationCustomerScans string = "kycBtcsIDVerificationResult"
)

type MessagePayload struct {
	Action struct {
		Code string `json:"code"`
	} `json:"action"`
}

func CronSubscribe(app *application.Application) {

	client, err := pubsubwrapper.NewClient(app.Config.ProjectID, app.Config.CronEventSubscriptionTopic)
	if err != nil {
		log.Error().Err(err).Msg("Failed to gcpClient")
		return
	}

	defer func() {
		err := client.CloseClient()
		if err != nil {
			log.Error().Err(err).Msg("Failed to close gcpClient")
		}
	}()

	topic, err := client.GetTopic(nil, app.Config.CronEventSubscriptionTopic)
	if err != nil {
		log.Error().Err(err).Msg("Failed get gcpTopic")
		return
	}

	defer topic.Stop()

	sub, err := client.GetSubscription(nil, topic)
	if err != nil {
		log.Error().Err(err).Msg("Failed GetSubscription")
		return
	}

	err = client.PullMessage(nil, sub.String(), topic, func(m *pubsub.Message) {
		log.Debug().Msgf("CronSubscribe -> message: %+s\n", string(m.Data))

		message := &MessagePayload{}
		err := json.Unmarshal(m.Data, message)
		if err != nil {
			log.Debug().Msgf("Invalid message event - %v", m.Data)
			return
		}

		switch message.Action.Code {
		case CreateCustomer:
			err := CreateBtcsCustomers(app)
			if err != nil {
				log.Error().Err(err).Msg("CretatedBtcsCustomers error")
			}
		case SendEthAddress:
			err := SendEthAddressBtcsCustomers(app)
			if err != nil {
				log.Error().Err(err).Msg("SendEthAddressBtcsCustomers error")
			}
		case UploadImagesScans:
			err := SendBTSCImages(app)
			if err != nil {
				log.Error().Err(err).Msg("SendBTSCImages error")
			}
		case VerificationCustomerScans:
			err := CheckVerificationCustomerScans(app)
			if err != nil {
				log.Error().Err(err).Msg("CheckVerificationCustomerScans error")
			}
		}

		return
	})

	if err != context.Canceled {
		log.Error().Err(err).Msg(err.Error())
	}
}

func CreateBtcsCustomers(app *application.Application) error {
	customers, _ := app.Repo.Customer.GetCustomersByBtcs(models.StateNew, models.WorkingNew)
	if len(customers) == 0 {
		return nil
	}

	tasks := make([]*pools.Task, 0)
	for _, customer := range customers {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task CretatedBtcsCustomers::message[%+s]", t.Message)

			app := t.App.(*application.Application)
			message := t.Message

			customer, err := app.Repo.Customer.GetCustomerByID(message.ID)
			if err != nil {
				return err
			}

			if customer.State == models.StateCreatedUser {
				return nil
			}

			err = app.Repo.Customer.SetCustomerInWork(models.WorkingInProcessing, customer)
			if err != nil {
				return err
			}

			fields := make(map[string]interface{})
			findCustomers, err := app.BTCSClient.FindCustomer(map[string]string{
				"primary_contact": customer.Email,
			})

			if err != nil {
				if errDb := app.Repo.Customer.SetCustomerInWork(models.WorkingNew, customer); errDb != nil {
					return errDb
				}

				return err
			}

			needCreatedBTCSCustomer := true
			if findCustomers.Count > 0 {
				var findCustomer btcsclient.GetCustomerResponse
				log.Debug().Msgf("[cron::CreateBtcsCustomers] findCustomers %+v", findCustomers)

				for _, item := range findCustomers.Customers {
					if len(customer.Phone) > 0 && item.IsUsedPhoneNumber(customer.Phone[0]) {
						findCustomer = item
						log.Debug().Msgf("[cron::CreateBtcsCustomers] findCustomer %+v", customer)
					}
				}

				if findCustomer.ID != 0 {
					needCreatedBTCSCustomer = false
					// Update Data custom in DB
					fields["kyc_id"] = strconv.Itoa(findCustomer.ID)
					fields["first_name"] = findCustomer.FirstName
					fields["last_name"] = findCustomer.LastName
					fields["dob"] = findCustomer.Dob
				}
			}

			if needCreatedBTCSCustomer {
				btcsCustomer := &btcsclient.CreateCustomerRequest{
					FirstName:      customer.FirstName,
					LastName:       customer.LastName,
					Email:          customer.Email,
					Phone:          customer.Phone,
					Dob:            customer.Dob,
					PrimaryContact: customer.Email,
					LookupKeys:     []string{customer.ID},
				}

				createdCustomer, err := app.BTCSClient.CreateCustomer(btcsCustomer)
				if err != nil {
					if errDb := app.Repo.Customer.SetCustomerInWork(models.WorkingNew, customer); errDb != nil {
						return errDb
					}

					return fmt.Errorf("error creating customer on BTCS: %+v Payload: %+v", err, btcsCustomer)
				}

				fields["kyc_id"] = strconv.Itoa(createdCustomer.ID)
			}

			fields["state"] = models.StateCreatedUser
			fields["in_work"] = models.WorkingNew
			err = app.Repo.Customer.UpdateCustomer(customer, fields)
			if err != nil {
				return fmt.Errorf("cannot update customer in DB: %+v", err)
			}

			return nil
		}, &pools.Message{ID: customer.ID}, app))
	}

	if err := initPools(tasks); err != nil {
		return err
	}

	return nil
}

func SendEthAddressBtcsCustomers(app *application.Application) error {
	customers, _ := app.Repo.Customer.GetCustomersByBtcs(models.StateCreatedUser, models.WorkingNew)
	if len(customers) == 0 {
		return nil
	}

	myEnv := env.GetEnv()
	callbackURL := myEnv["SERVICE_DOMAIN"]
	callbackURI := app.FindRoute("btcs_address_events")
	callback := callbackURL + callbackURI

	tasks := make([]*pools.Task, 0)
	for _, customer := range customers {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task SendEthAddressBtcsCustomers::message[%+s]", t.Message)
			message := t.Message
			app := t.App.(*application.Application)

			customer, err := app.Repo.Customer.GetCustomerByID(message.ID)
			if err != nil {
				return err
			}

			if customer.State == models.StateSaveEthAddress {
				return nil
			}

			err = app.Repo.Customer.SetCustomerInWork(models.WorkingInProcessing, customer)
			if err != nil {
				return err
			}

			submitAddressRequest := &btcsclient.SubmitAddress{Address: customer.EthAddress, CallbackURL: message.Callback}
			err = app.BTCSClient.SubmitAddress(submitAddressRequest, customer.KycID)
			if err != nil {
				errDb := app.Repo.Customer.SetCustomerInWork(models.WorkingNew, customer)
				if errDb != nil {
					return errDb
				}

				return fmt.Errorf("error submitting addres to BTCS: %+v. Payload: %+v", err, submitAddressRequest)
			}

			fields := make(map[string]interface{})
			fields["state"] = models.StateSaveEthAddress
			fields["in_work"] = models.WorkingNew

			err = app.Repo.Customer.UpdateCustomer(customer, fields)
			if err != nil {
				return fmt.Errorf("can not update customer in DB: %+v", err)
			}

			return nil
		}, &pools.Message{ID: customer.ID, Callback: callback}, app))
	}

	if err := initPools(tasks); err != nil {
		return err
	}

	return nil
}

func SendBTSCImages(app *application.Application) error {
	customers, _ := app.Repo.Customer.GetCustomersByBtcsImages(models.StateSaveEthAddress, models.WorkingNew, models.ImagesFailed)
	if len(customers) == 0 {
		return nil
	}

	tasks := make([]*pools.Task, 0)
	for _, customer := range customers {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task SendBTSCImages::message[%+s]", t.Message)
			message := t.Message
			app := t.App.(*application.Application)

			customer, err := app.Repo.Customer.GetCustomerByID(message.ID)
			if err != nil {
				return err
			}

			if customer.State == models.StateSendImages {
				return nil
			}

			err = app.Repo.Customer.SetCustomerInWork(models.WorkingInProcessing, customer)
			if err != nil {
				return err
			}

			if app.Repo.Customer.DoesAllImageFlagsAreSet(customer) {
				err := customer_service.SubmitCustomerIDScansFromS3Service(app, customer.ID)
				if err != nil {
					errDb := app.Repo.Customer.SetCustomerInWork(models.WorkingNew, customer)
					if errDb != nil {
						return errDb
					}

					return fmt.Errorf("error submitting images to BTCS: %+v", err)
				}

				fields := make(map[string]interface{})
				fields["state"] = models.StateSendImages
				fields["in_work"] = models.WorkingNew
				err = app.Repo.Customer.UpdateCustomer(customer, fields)
				if err != nil {
					return fmt.Errorf("can not update customer in DB: %+v", err)
				}

				return nil
			}

			err = app.Repo.Customer.SetCustomerInWork(models.WorkingNew, customer)
			if err != nil {
				return err
			}

			return nil
		}, &pools.Message{ID: customer.ID}, app))
	}

	if err := initPools(tasks); err != nil {
		return err
	}

	return nil
}

func CheckVerificationCustomerScans(app *application.Application) error {
	customers, err := app.Repo.Customer.GetCustomersByNotVerification(
		models.ImagesSuccess,
		models.WorkingNew,
		models.CheckScansNew)

	if err != nil {
		log.Error().Err(err).Msgf("CheckVerificationCustomerScans error")
		return err
	}

	if len(customers) == 0 {
		return nil
	}

	tasks := make([]*pools.Task, 0)
	for _, customer := range customers {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task CheckVerificationCustomerScans::message[%+s]", t.Message)
			message := t.Message
			app := t.App.(*application.Application)

			customer, err := app.Repo.Customer.GetCustomerByID(message.ID)
			if err != nil {
				return err
			}

			err = app.Repo.Customer.SetCustomerIsCronCheckScans(models.CheckScansDone, customer)
			if err != nil {
				return err
			}

			err = NotifyLKDBackednAboutIDVereficationEvent(app, customer)
			if err != nil {
				errDb := app.Repo.Customer.SetCustomerIsCronCheckScans(models.CheckScansNew, customer)
				if errDb != nil {
					return errDb
				}
			}

			return nil
		}, &pools.Message{ID: customer.ID}, app))
	}

	if err := initPools(tasks); err != nil {
		return err
	}

	return nil
}

func initPools(tasks []*pools.Task) error {
	c, err := strconv.Atoi(os.Getenv("CRON_CONCURRENCY_WORKER"))
	if err != nil {
		return err
	}

	durationTimeEnv := os.Getenv("CRON_TIME_DURATION_TASKS")
	durationTime, err := strconv.ParseInt(durationTimeEnv, 10, 64)
	if err != nil {
		return fmt.Errorf("cannot validate type: %T : %v", durationTime, err)
	}

	p, err := pools.NewPool(tasks, c, durationTime)
	if err != nil {
		return err
	}
	p.Run()

	for _, task := range p.Tasks {
		if task.Err != nil {
			log.Error().Err(task.Err).Msg("Task error msg.")
		}
	}

	return nil
}

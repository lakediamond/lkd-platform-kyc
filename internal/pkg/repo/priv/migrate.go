package priv

import (
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"
	"gopkg.in/gormigrate.v1"

	"kyc-service/internal/pkg/repo/priv/migrations"
)

// Migration to create actual database version
func Migration(db *gorm.DB) {

	db.LogMode(true)

	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		&migrations.CreateCustomerTable,
		&migrations.AddCustomerInfoField,
		&migrations.AddCustomerSendBtcsImagesStatusField,
		&migrations.AddCustomerWebHookCheckScansField,
		&migrations.RemoveCustomerKycIdUniqueField,
		&migrations.AddIndexCustomers,
	})

	if err := m.Migrate(); err != nil {
		log.Error().Msgf("Could not migrate: %v", err)
	}

	log.Info().Msg("Migration did run successfully")
}

package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddCustomerSendBtcsImagesStatusField = gormigrate.Migration{
	ID: "201902261646",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE customers 
ADD IF NOT EXISTS status_btcs_images INTEGER DEFAULT 0
;
`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE customers 
		DROP COLUMN IF EXISTS status_btcs_images;`
		return tx.Exec(req).Error
	},
}

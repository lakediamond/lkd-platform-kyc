package lkdbackclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/rs/zerolog/log"

	"kyc-service/pkg/btcsclient"
)

type client struct {
	BaseURL    *url.URL
	HTTPClient *http.Client
}

// NewLKDBackCLient is the LKDB back client factory
func NewLKDBackCLient(baseURL string) LKDBackClient {
	httpClient := http.Client{Timeout: time.Second * 30}

	url, _ := url.Parse(baseURL)

	return &client{BaseURL: url, HTTPClient: &httpClient}
}

func (c *client) NotifyBTCSFlowStarted(customerID string) error {
	log.Info().Msgf("Notifying LKD back BTCS flow has been started for customer: %v", customerID)
	uri := fmt.Sprintf("/user/kyc/flow/%v", customerID)

	req, err := c.makeRequest(http.MethodPost, uri, nil, nil)
	if err != nil {
		return err
	}

	_, err = c.do(req, nil)
	if err != nil {
		return err
	}

	return nil
}

func (c *client) SubmitIDVerificationResults(results *btcsclient.BtcsScansEventIDVerificationsResult, customerID string) error {
	log.Info().Msgf("Submitting ID verification results to LKD back. Results: %+v. Customer: %v", results, customerID)

	uri := fmt.Sprintf("/user/kyc/verification/%v/id", customerID)

	req, err := c.makeRequest(http.MethodPost, uri, results, nil)
	if err != nil {
		return err
	}

	resp, err := c.HTTPClient.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}

	if err != nil {
		return err
	}

	if !(resp.StatusCode >= 200 && resp.StatusCode <= 299) {
		return fmt.Errorf("LKD platform response status: %d", resp.StatusCode)
	}

	return nil
}

func (c *client) SubmitIDProcessingError(message, customerID string) error {
	log.Info().Msgf("Submitting ID verefication error to LKD back. Error: %+v. Customer: %v", message, customerID)

	results := &btcsclient.BtcsScansEventIDVerificationsResult{
		VerificationStatus:   "PROCESSING_ERROR",
		IdentityVerification: fmt.Sprintf("Following item is invalid, %s", message),
		CallbackDate:         time.Now(),
	}

	uri := fmt.Sprintf("/user/kyc/verification/%v/id/%s", customerID, message)

	req, err := c.makeRequest(http.MethodDelete, uri, results, nil)
	if err != nil {
		return err
	}

	_, err = c.do(req, nil)
	if err != nil {
		return err
	}

	return nil
}

func (c *client) makeRequest(method, path string, body interface{}, query map[string]string) (*http.Request, error) {
	uri := &url.URL{Path: path}
	u := c.BaseURL.ResolveReference(uri)
	q := u.Query()
	if query != nil {
		for k, v := range query {
			q.Set(k, v)
		}
	}
	c.BaseURL.Path = path
	u.RawQuery = q.Encode()
	var buf = new(bytes.Buffer)
	if body != nil {
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(method, u.String(), buf)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	return req, nil
}

func (c *client) do(req *http.Request, target interface{}) (*http.Response, error) {
	resp, err := c.HTTPClient.Do(req) // HTTPClient
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if !(resp.StatusCode >= 200 && resp.StatusCode <= 299) {
		badResponseError := &LkdBackClientErrors{Code: resp.StatusCode}
		err = json.NewDecoder(resp.Body).Decode(badResponseError)
		return resp, badResponseError
	}

	fmt.Printf("resp status: %+s", resp.StatusCode)

	err = json.NewDecoder(resp.Body).Decode(target)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *client) SubmitSuccessImageValidation(customerID, imageType string) error {
	log.Info().Msgf("Submitting image verification result to LKD back. Customer: %s, imageType: %s", customerID, imageType)
	uri := fmt.Sprintf("/user/kyc/verification/%s/id/%s", customerID, imageType)
	req, err := c.makeRequest(http.MethodPatch, uri, nil, nil)
	if err != nil {
		return err
	}

	resp, err := c.HTTPClient.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}

	if err != nil {
		return err
	}

	if !(resp.StatusCode >= 200 && resp.StatusCode <= 299) {
		return fmt.Errorf("LKD platform response status: %d", resp.StatusCode)
	}

	return nil
}

package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var RemoveCustomerKycIdUniqueField = gormigrate.Migration{
	ID: "201903051309",
	Migrate: func(tx *gorm.DB) error {
		var req = `ALTER TABLE customers DROP CONSTRAINT customers_kyc_id_key;`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `CREATE UNIQUE INDEX customers_kyc_id_key ON customers (kyc_id);`
		return tx.Exec(req).Error
	},
}

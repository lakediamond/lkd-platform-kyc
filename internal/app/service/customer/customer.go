package customer

import (
	"errors"
	"fmt"
	"io"
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo/models"
	repo_utils "kyc-service/internal/pkg/repo/utils"
	"kyc-service/internal/pkg/utils"
	"kyc-service/pkg/btcsclient"
	"kyc-service/pkg/env"
	"mime/multipart"
	"os"

	"github.com/rs/zerolog/log"
)

func getCustomerLevels(app *application.Application, id string) (*btcsclient.CustomerLevels, error) {

	kycID, err := app.Repo.Customer.GetCustomerKycIDByID(id)
	if err != nil {
		return nil, err
	}
	customerLevels, err := app.BTCSClient.GetCustomerLevels(kycID)
	if err != nil {
		return nil, err
	}

	return customerLevels, nil
}

func createCustomerService(app *application.Application, customerData *CreateCustomerData) error {
	phoneNumbers := make([]string, 0)

	if customerData.ID == "" {
		return &repo_utils.NotNullConstrainError{Field: "ID", Table: "Customer"}
	}

	_, err := app.Repo.Customer.GetCustomerKycIDByID(customerData.ID)
	if err == nil {
		log.Error().Msgf("Error creating customer: %+v. ID already exists", customerData.ID)
		return &repo_utils.DuplicateConstraintError{Field: "ID", Table: "Customer"}
	}

	phoneNumbers = customerData.Phone
	if phoneTemplate, ok := os.LookupEnv("KYC_CUSTOMER_PHONE_NUMBER_TEMPLATE"); ok && len(phoneTemplate) > 0 {
		customersNb := app.Repo.Customer.GetCustomerRecordsCount()
		btcsFakePhone := utils.NextBtcsPhoneNumber(customersNb)
		log.Info().Msgf("customers nb from db - %s, phone value passed to Btcs - %s", customersNb, btcsFakePhone[0])
		phoneNumbers = btcsFakePhone
	}

	customer := &models.Customer{
		ID:         customerData.ID,
		FirstName:  customerData.FirstName,
		LastName:   customerData.LastName,
		Email:      customerData.Email,
		Phone:      phoneNumbers,
		Dob:        customerData.Dob,
		EthAddress: customerData.EthAddress,
	}

	log.Info().Msgf("CreateNewCustomer struct: %+s", customer)

	err = app.Repo.Customer.CreateNewCustomer(customer)
	if err != nil {
		log.Error().Err(err).Msgf("Error creating customer in DB.")
		return err
	}

	return nil
}

func updateCustomerService(app *application.Application, customerData *btcsclient.UpdateCustomerRequest, customerID string) error {

	kycID, err := app.Repo.Customer.GetCustomerKycIDByID(customerID)
	if err != nil {
		return err
	}

	_, err = app.BTCSClient.UpdateCustomer(customerData, kycID)

	if err != nil {
		return err
	}

	return nil
}

func resetMediaUploadStatusService(app *application.Application, customerID string, mediaType models.MediaType) error {
	customer, err := app.Repo.Customer.GetCustomerByID(customerID)
	if err != nil {
		return err
	}

	if !customer.IsValidMediaType(mediaType) {
		return errors.New("invalid media type token provided")
	}

	err = app.Repo.Customer.SetImageFlag(false, mediaType, customer)
	if err != nil {
		return err
	}

	return nil
}

func setCustomerPhoneSmsService(app *application.Application, customerID string) error {
	kycID, err := app.Repo.Customer.GetCustomerKycIDByID(customerID)
	if err != nil {
		return err
	}

	err = app.BTCSClient.VerifyPhoneSms(kycID)
	if err != nil {
		return err
	}

	return nil
}

func submitCustomerIDScansService(app *application.Application, m *multipart.Form, customerID string) error {
	log.Info().Msgf("submitting images for customer: %v", customerID)
	faceImage, _ := m.File["face_image"][0].Open()
	frontImage, _ := m.File["front_image"][0].Open()
	backImage, _ := m.File["back_image"][0].Open()
	defer faceImage.Close()
	defer frontImage.Close()
	defer backImage.Close()

	err := submitImagestoBTCS(app, faceImage, backImage, frontImage, customerID)
	if err != nil {
		log.Error().Err(err).Msgf("Error submitting images for customer: %v.", customerID)
		return err
	}
	return nil
}

// SubmitCustomerIDScansFromS3Service fetch ALL images from S3 and submits them to BTCS
func SubmitCustomerIDScansFromS3Service(app *application.Application, customerID string) error {
	log.Info().Msgf("submitting images for customer: %v", customerID)

	customer, _ := app.Repo.Customer.GetCustomerByID(customerID)
	keyFace := fmt.Sprintf("%v/identity-scans/%v.%v", customerID, "face_image", customer.FaceFormat)
	keyFront := fmt.Sprintf("%v/identity-scans/%v.%v", customerID, "front_image", customer.FrontFormat)
	keyBack := fmt.Sprintf("%v/identity-scans/%v.%v", customerID, "back_image", customer.BackFormat)

	frontImageReader, err := app.Storage.GetImageReader(customer.ID, keyFront)
	if err != nil {
		return err
	}

	backImageReader, err := app.Storage.GetImageReader(customer.ID, keyBack)
	if err != nil {
		return err
	}

	faceImageReader, err := app.Storage.GetImageReader(customer.ID, keyFace)
	if err != nil {
		return err
	}

	err = submitImagestoBTCS(app,
		frontImageReader,
		backImageReader,
		faceImageReader,
		customerID)
	if err != nil {
		if errDb := app.Repo.Customer.SetCustomerStatusImages(models.ImagesFailed, customer); errDb != nil {
			return errDb
		}

		log.Error().Err(err).Msgf("Error submitting images for customer: %v.", customerID)
		return err
	}

	if err := app.Repo.Customer.SetCustomerStatusImages(models.ImagesSuccess, customer); err != nil {
		return err
	}

	return nil
}

func submitImagestoBTCS(app *application.Application, faceImage, backImage, frontImage io.Reader, customerID string) error {
	imageProcessor := &btcsclient.MultiPartImagesProcessor{
		BackImage:  backImage,
		FrontImage: frontImage,
		FaceImage:  faceImage,
		BtcsClient: app.BTCSClient,
	}

	kycID, err := app.Repo.Customer.GetCustomerKycIDByID(customerID)
	if err != nil {
		return err
	}
	myEnv := env.GetEnv()
	callbackURL := myEnv["SERVICE_DOMAIN"]
	//TODO: check why it breaks
	// callbackURI := app.FindRoute("btcs_scan_events")
	callbackURI := "/api/events/btcs/scans"
	callback := callbackURL + callbackURI

	err = imageProcessor.PrepareUploadSubmitAllScans(kycID, callback)
	if err != nil {
		_, ok := err.(*btcsclient.ImageProcessorError)
		if ok {
			_ = app.LKDBackClient.SubmitIDProcessingError(err.Error(), customerID)
		}
		return err
	}

	return nil
}

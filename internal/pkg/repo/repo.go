package repo

import (
	"fmt"
	"kyc-service/pkg/env"
	"kyc-service/pkg/gormlogger"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/rs/zerolog/log"
)

//Repo contains all table repositories
type Repo struct {
	DB       *gorm.DB
	Customer CustomerRepository
}

// GetDbClient returns DB connection
func GetDbClient() *Repo {
	myEnv := env.GetEnv()

	dbName := myEnv["DATABASE_NAME"]
	dbUser := myEnv["DATABASE_USER"]
	dbPass := myEnv["DATABASE_PASSWORD"]
	dbHost := myEnv["DATABASE_HOST"]
	dbPort := myEnv["DATABASE_PORT"]

	db, err := gorm.Open("postgres", fmt.Sprintf(
		"user=%s password=%s dbname=%s host=%s port=%s sslmode=disable",
		dbUser, dbPass, dbName, dbHost, dbPort))
	if err != nil {
		log.Panic().Msg(err.Error())
	}

	db.SetLogger(&gormlogger.GormLogger{})

	db.LogMode(true)

	return &Repo{
		db,
		&CustomerRepo{db},
	}
}

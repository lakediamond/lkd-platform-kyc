package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddCustomerWebHookCheckScansField = gormigrate.Migration{
	ID: "201902271409",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE customers 
ADD IF NOT EXISTS is_web_hook INTEGER DEFAULT 0,
ADD IF NOT EXISTS is_cron_check_scans INTEGER DEFAULT 0
;
`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE customers 
		DROP COLUMN IF EXISTS is_web_hook,
		DROP COLUMN IF EXISTS is_cron_check_scans;`
		return tx.Exec(req).Error
	},
}

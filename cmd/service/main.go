package main

import (
	"github.com/rs/zerolog/log"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
	_ "image/jpeg"
	"kyc-service/pkg/env"
	"net"
	"os"

	"kyc-service/internal/app/service/events"
	"kyc-service/internal/app/service/httpserver"
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo"
	"kyc-service/internal/pkg/utils"
)

func main() {
	if env.CheckTracerRun(os.Getenv("ENV")) {
		log.Info().Msg("Start Tracer")
		addr := net.JoinHostPort(
			os.Getenv("DD_AGENT_HOST"),
			os.Getenv("DD_TRACE_AGENT_PORT"),
		)
		tracer.Start(tracer.WithAgentAddr(addr), tracer.WithServiceName("lkd-kyc-service"))
		defer tracer.Stop()
	}

	app := application.NewKycService()
	app.Repo = repo.GetDbClient()
	app.BTCSClient = utils.NewBTCSClient()
	app.LKDBackClient = utils.NewLKDBackClient()
	app.Storage = utils.NewStorage()

	sub, present := os.LookupEnv("RUN_SUBSCRIBERS")
	if present && sub == "true" {
		go events.Subscribe(app)
	}

	cronSub, present := os.LookupEnv("CRON_RUN_SUBSCRIBERS")
	if present && cronSub == "true" {
		go events.CronSubscribe(app)
	}

	httpserver.Serve(app)
}

package btcsclient_test

import (
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/testutils"
	"kyc-service/internal/pkg/utils"
	"kyc-service/pkg/btcsclient"
	"net/http/httptest"
	"testing"

	// "github.com/stretchr/testify/assert"
	_ "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type BtcsClientTestSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
	BTCSClient btcsclient.BTCSClient
}

func TestCheckIdentitySuite(t *testing.T) {
	suite.Run(t, new(BtcsClientTestSuite))
}

func (suite *BtcsClientTestSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	suite.App = app
	suite.MockServer = mockServer
	suite.BTCSClient = utils.NewBTCSClient()
}

//func (suite *BtcsClientTestSuite) TestGetCustomerLevels() {
//	resp, err := suite.BTCSClient.GetCustomerLevels("5138008542019584")
//	if assert.Nil(suite.T(), err) {
//		assert.NotNil(suite.T(), resp)
//	}
//}

// func (suite *BtcsClientTestSuite) TestCreateCustomer() {
// 	target, err := suite.BTCSClient.CreateCustomer(&btcsclient.CreateCustomerRequest{FirstName: "Kirill",
// 		LastName: "Pisariev", Email: "k.pisarev+1@edenlab.com.ua", Phone: []string{"+380666776780"},
// 		PostalCode: "123sdsdas", Country: "UKR", State: "Kyiv", Address1: "Tarasa Shevchenka", Dob: "1985-04-12", PrimaryContact: "k.pisarev+1@edenlab.com.ua",
// 	})
// 	if assert.Nil(suite.T(), err) {
// 		assert.NotNil(suite.T(), target)
// 	}
// }
//
//func (suite *BtcsClientTestSuite) TestUpdateCustomer() {
//	target, err := suite.BTCSClient.UpdateCustomer(&btcsclient.UpdateCustomerRequest{
//		LastName: "Pisariev123321",
//	}, "5138008542019584")
//	if assert.Nil(suite.T(), err) {
//		assert.NotNil(suite.T(), target)
//		assert.Equal(suite.T(), target.LastName, strings.ToUpper("Pisariev123"), "Updated last name should be equal")
//	}
//}

// func (suite *BtcsClientTestSuite) TestGetCustomerLimits() {
// 	target, err := suite.BTCSClient.GetCustomerLimits("5724044800294912")
// 	if assert.Nil(suite.T(), err) {
// 		assert.NotNil(suite.T(), target)
// 	}
// }

// func (suite *BtcsClientTestSuite) TestGetOrgLevels() {
// 	target, err := suite.BTCSClient.GetOrganizationLevels()
// 	if assert.Nil(suite.T(), err) {
// 		assert.NotNil(suite.T(), target)
// 	}
// }

// func (suite *BtcsClientTestSuite) TestSubmitAddress() {
// 	err := suite.BTCSClient.SubmitAddress(&btcsclient.SubmitAddress{
// 		Address: "0x123", CallbackURL: "https://05d8adb0.ngrok.io/api/events/btcs/scans",
// 	}, "5750770301403136")
// 	suite.Nil(err)
// }

// func (suite *BtcsClientTestSuite) TestGetIDVereficationTraceLogs() {
// 	resp, err := suite.BTCSClient.GetIDVereficationTraceLogs("5750770301403136")
// 	suite.Nil(err)
// }

func (suite *BtcsClientTestSuite) TestGetTraces() {
	resp, err := suite.BTCSClient.GetCustomerTaces("5630286201094144")
	suite.Nil(err)
	suite.NotNil(resp.Traces[0].RawData.Matches)
	suite.Equal(resp.Traces[0].Remarks, "VERIFIED_NAME_SANCTIONS_SCREEN set to False")
	matches := btcsclient.FindSanctionsLogsInCustomerTraces(resp)
	suite.NotNil(matches)
}

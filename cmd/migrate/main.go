package main

import (
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo"
	"kyc-service/internal/pkg/repo/priv"
)

func main() {
	app := application.NewKycService()
	app.Repo = repo.GetDbClient()
	priv.Migration(app.Repo.DB)
}

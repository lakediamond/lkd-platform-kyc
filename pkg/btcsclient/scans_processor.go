package btcsclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image"
	"image/jpeg"
	_ "image/jpeg"
	_ "image/png"
	"io"
	// "mime/multipart"
)

// MultiPartImagesProcessor is the struct that represents methods and state required to upload images to btcs
type MultiPartImagesProcessor struct {
	BackFilename  string `json:"back_filename"`
	BackURL       string `json:"back_url"`
	FaceFilename  string `json:"face_filename"`
	FaceURL       string `json:"face_url"`
	FrontFilename string `json:"front_filename"`
	FrontURL      string `json:"front_url"`

	BackImage  io.Reader
	FrontImage io.Reader
	FaceImage  io.Reader

	BtcsClient BTCSClient
}

// PrepareAccess function fetch urls and filenames to upload images to BTCS
func (proc *MultiPartImagesProcessor) PrepareAccess(customerID string) error {
	scansAccess, err := proc.BtcsClient.RequestIDScansAccess(customerID)
	if err != nil {
		return err
	}
	proc.BackFilename = scansAccess.BackFilename
	proc.BackURL = scansAccess.BackURL
	proc.FaceFilename = scansAccess.FaceFilename
	proc.FaceURL = scansAccess.FaceURL
	proc.FrontFilename = scansAccess.FrontFilename
	proc.FrontURL = scansAccess.FrontURL
	return nil
}

func (proc *MultiPartImagesProcessor) CheckFaceImageValidity() error {
	_, err := proc.encodeImage(proc.FaceImage, "face_image")
	if err != nil {
		return err
	}

	return nil
}

func (proc *MultiPartImagesProcessor) CheckFrontImageValidity() error {
	_, err := proc.encodeImage(proc.FrontImage, "front_image")
	if err != nil {
		return err
	}

	return nil
}

func (proc *MultiPartImagesProcessor) CheckBackImageValidity() error {
	_, err := proc.encodeImage(proc.BackImage, "back_image")
	if err != nil {
		return err
	}

	return nil
}

// UploadFace uploads face_image to the link provided by BTCS
func (proc *MultiPartImagesProcessor) UploadFace() error {
	buffer, err := proc.encodeImage(proc.FaceImage, "face_image")
	if err != nil {
		return err
	}

	err = proc.BtcsClient.UploadIDScan(proc.FaceURL, buffer)
	if err != nil {
		return err
	}

	return nil
}

// UploadFront uploads front_image to the link provided by BTCS
func (proc *MultiPartImagesProcessor) UploadFront() error {
	buffer, err := proc.encodeImage(proc.FrontImage, "front_image")
	if err != nil {
		return err
	}

	err = proc.BtcsClient.UploadIDScan(proc.FrontURL, buffer)
	if err != nil {
		return err
	}

	return nil
}

// UploadBack uploads back_image to the link provided by BTCS
func (proc *MultiPartImagesProcessor) UploadBack() error {
	buffer, err := proc.encodeImage(proc.BackImage, "back_image")
	if err != nil {
		return err
	}

	err = proc.BtcsClient.UploadIDScan(proc.BackURL, buffer)
	if err != nil {
		return err
	}

	return nil
}

// SubmitScans submits prepared and uploaded images to BTCS
func (proc *MultiPartImagesProcessor) SubmitScans(customerID string, callback string) error {
	scansEntety := &UploadIDScansRequest{
		FaceImage:   proc.FaceFilename,
		FrontImage:  proc.FrontFilename,
		BackImage:   proc.BackFilename,
		CallBackURL: callback,
	}

	_, err := proc.BtcsClient.SubmitIDScans(scansEntety, customerID)
	if err != nil {
		return err
	}

	return nil
}

// PrepareUploadSubmitAllScans prepares access upload and submit images to btcs
func (proc *MultiPartImagesProcessor) PrepareUploadSubmitAllScans(customerID, callback string) error {
	err := proc.PrepareAccess(customerID)
	if err != nil {
		return err
	}

	err = proc.UploadBack()
	if err != nil {
		return err
	}

	err = proc.UploadFace()
	if err != nil {
		return err
	}

	err = proc.UploadFront()
	if err != nil {
		return err
	}

	err = proc.SubmitScans(customerID, callback)
	if err != nil {
		return err
	}

	return nil
}

func (proc *MultiPartImagesProcessor) encodeImage(file io.Reader, filename string) (io.Reader, error) {
	img, f, err := proc.decodeImage(file, filename)
	if err != nil {
		return nil, err
	}

	buffer := new(bytes.Buffer)
	if err := jpeg.Encode(buffer, img, &jpeg.Options{Quality: 60}); err != nil {
		err := &ImageProcessorError{Code: "EncodeImageError", ErrorMessage: fmt.Sprintf("%v is invalid, %v.%v", filename, filename, f)}
		return nil, err
	}
	return buffer, nil
}

func (proc *MultiPartImagesProcessor) decodeImage(file io.Reader, filename string) (image.Image, string, error) {
	i, f, err := image.Decode(file)
	if err != nil {
		err := &ImageProcessorError{Code: "DecodeImageError", ErrorMessage: fmt.Sprintf("%v is invalid, %v.%v", filename, filename, f)}
		return nil, f, err
	}
	return i, f, nil
}

type ImageProcessorError struct {
	Code         string `json:"error"`
	ErrorMessage string `json:"message"`
}

func (e *ImageProcessorError) Error() string {
	return fmt.Sprintf("%+v", e.ErrorMessage)
}

func (e ImageProcessorError) MarshalJSON() ([]byte, error) {
	errors := make(map[string]string)
	errors["Error"] = e.Code
	errors["Message"] = e.Error()
	return json.Marshal(errors)
}

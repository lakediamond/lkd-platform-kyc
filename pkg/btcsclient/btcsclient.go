package btcsclient

import (
	"io"
	"net/http"
)

// BTCSClient is the representation of btcsclient application
type BTCSClient interface {
	GetCustomer(string) (*GetCustomerResponse, error)
	GetCustomerLevels(string) (*CustomerLevels, error)
	GetCustomerLimits(string) (*CustomerLimits, error)
	GetCustomerTaces(customerID string) (*CustomerTraces, error)
	CreateCustomer(*CreateCustomerRequest) (*GetCustomerResponse, error)
	UpdateCustomer(body *UpdateCustomerRequest, customerID string) (*GetCustomerResponse, error)
	SubmitAddress(body *SubmitAddress, customerID string) error
	RequestIDScansAccess(customerID string) (*ScanAccessResponse, error)
	UploadIDScan(url string, image io.Reader) error
	SubmitIDScans(body *UploadIDScansRequest, customerID string) (*GetCustomerResponse, error)
	GetOrganizationLevels() (*OrganizationLevels, error)
	GetIDVerificationTraceLogs(customerID string) (*BTCSIDVerificationEventsResponse, error)
	FindCustomer(query map[string]string) (*GetCustomersResponse, error)
	VerifyPhoneSms(customerID string) error
	do(req *http.Request, target interface{}) (*http.Response, error)
	makeRequest(method, path string, body interface{}, query map[string]string) (*http.Request, error)
}

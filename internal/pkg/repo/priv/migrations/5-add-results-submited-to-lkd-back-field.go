package migrations

import (
	"kyc-service/internal/pkg/repo/models"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddVerificationResultsSubmitedField = gormigrate.Migration{
	ID: "cd28d2b0-083e-11e9-b568-0800200c9a66",
	Migrate: func(tx *gorm.DB) error {
		error := tx.AutoMigrate(&models.Customer{}).Error
		if error != nil {
			return error
		}
		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE customers 
		DROP COLUMN scans_check_submited_at;`
		return tx.Exec(req).Error
	},
}

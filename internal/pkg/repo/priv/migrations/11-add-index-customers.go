package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddIndexCustomers = gormigrate.Migration{
	ID: "201903141202",
	Migrate: func(tx *gorm.DB) error {
		var req = `
			CREATE INDEX customers_is_web_hook_idx ON customers(is_web_hook);
			CREATE INDEX customers_is_cron_check_scans_idx ON customers(is_cron_check_scans);	
			CREATE INDEX customers_kyc_id_idx ON customers(kyc_id);
			CREATE INDEX customers_state_in_work_idx ON customers(state,in_work);
			CREATE INDEX customers_state_in_work_status_btcs_images_idx ON customers(state,in_work,status_btcs_images);
		`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `
			DROP INDEX customers_is_web_hook_idx;
			DROP INDEX customers_is_cron_check_scans_idx;
			DROP INDEX customers_kyc_id_idx;
			DROP INDEX customers_state_in_work_idx;
			DROP INDEX customers_state_in_work_status_btcs_images_idx;
		`
		return tx.Exec(req).Error
	},
}

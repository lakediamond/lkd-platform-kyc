package btcsclient

import (
	"crypto/sha256"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

type AuthClient struct {
	APIKey    string
	APISecret string
}

func (ac AuthClient) getNonce() string {
	now := time.Now()
	unixNano := now.UnixNano()
	nonce := unixNano / 1000
	return strconv.FormatInt(nonce, 10)
}

func (ac AuthClient) getDataHash(nonce string) string {
	hasher := sha256.New()
	hashString := ac.APIKey + ac.APISecret + nonce
	hasher.Write([]byte(hashString))
	return fmt.Sprintf("%x", hasher.Sum(nil))
}

func (ac AuthClient) putAuthHeaders(r *http.Request) error {
	nonce := ac.getNonce()
	r.Header.Set("HTTP_API_KEY", ac.APIKey)
	r.Header.Set("HTTP_API_SIGN", ac.getDataHash(nonce))
	r.Header.Set("NONCE", nonce)

	return nil
}

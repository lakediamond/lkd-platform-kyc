package lkdbackclient

import (
	"kyc-service/pkg/btcsclient"
	"net/http"
)

// MockedLKDBackClient is an mock type for the LKDBackClient type
type MockedLKDBackClient struct {
	Response interface{}
	Error    error
}

// SubmitIDVerificationResults provides a mock function with given fields: results, customerID
func (_m *MockedLKDBackClient) SubmitIDVerificationResults(results *btcsclient.BtcsScansEventIDVerificationsResult, customerID string) error {
	// response, _ := _m.Response.(GetCustomerResponse)

	if _m.Error != nil {
		return _m.Error
	}

	return nil
}

// SubmitIDProcessingError provides a mock function with given fields: filename, fileformat, customerID
func (_m *MockedLKDBackClient) SubmitIDProcessingError(message, customerID string) error {
	if _m.Error != nil {
		return _m.Error
	}

	return nil
}

// do provides a mock function with given fields: req, target
func (_m *MockedLKDBackClient) do(req *http.Request, target interface{}) (*http.Response, error) {
	response, _ := _m.Response.(http.Response)

	if _m.Error != nil {
		return nil, _m.Error
	}

	return &response, nil
}

// makeRequest provides a mock function with given fields: method, path, body
func (_m *MockedLKDBackClient) makeRequest(method string, path string, body interface{}, query map[string]string) (*http.Request, error) {
	response, _ := _m.Response.(http.Request)

	if _m.Error != nil {
		return nil, _m.Error
	}

	return &response, nil
}

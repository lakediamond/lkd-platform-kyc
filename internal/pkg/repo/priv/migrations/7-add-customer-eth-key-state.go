package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddCustomerInfoField = gormigrate.Migration{
	ID: "201902261645",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE customers 
ADD IF NOT EXISTS eth_address TEXT, 
ADD IF NOT EXISTS first_name TEXT, 
ADD IF NOT EXISTS last_name TEXT,
ADD IF NOT EXISTS email TEXT,
ADD IF NOT EXISTS phone VARCHAR(22)[],
ADD IF NOT EXISTS dob TEXT,
ADD IF NOT EXISTS state INTEGER DEFAULT 0,
ADD IF NOT EXISTS in_work INTEGER DEFAULT 0
;
`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE customers 
		DROP COLUMN IF EXISTS eth_address, 
		DROP COLUMN IF EXISTS first_name, 
		DROP COLUMN IF EXISTS last_name, 
		DROP COLUMN IF EXISTS email, 
		DROP COLUMN IF EXISTS phone, 
		DROP COLUMN IF EXISTS dob,
		DROP COLUMN IF EXISTS state, 
		DROP COLUMN IF EXISTS in_work;`
		return tx.Exec(req).Error
	},
}

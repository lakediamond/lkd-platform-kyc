package customer

import (
	"encoding/json"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"

	"kyc-service/internal/pkg/context"
	repo_utils "kyc-service/internal/pkg/repo/utils"
	"kyc-service/pkg/btcsclient"
	"kyc-service/pkg/handlers"
)

// CreateCustomerData  is the structure that represents createCustomer request json data
type CreateCustomerData struct {
	FirstName  string   `json:"first_name"`
	LastName   string   `json:"last_name"`
	Email      string   `json:"email"`
	Phone      []string `json:"phone"`
	Dob        string   `json:"dob"`
	EthAddress string   `json:"eth_address"`
	ID         string   `json:"id"`
}

// VerifyCustomerHandler responsible for handling & validating HTTP request
var VerifyCustomerHandler = handlers.ApplicationHandler(verifyCustomer)

func verifyCustomer(w http.ResponseWriter, r *http.Request) {
	routeParams := httprouter.ParamsFromContext(r.Context())
	customerID := routeParams.ByName("customer_id")
	app := context.GetApplicationContext(r)

	customerLevels, err := getCustomerLevels(app, customerID)
	if err != nil {
		jsErr, _ := json.Marshal(err)
		if _, ok := err.(*repo_utils.DoesNotExistsError); ok {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(jsErr)
		return
	}

	jsonString, _ := json.Marshal(customerLevels)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonString)
	w.WriteHeader(http.StatusOK)
	return
}

// CreateCustomerHandler responsible for handling & validating HTTP request
var CreateCustomerHandler = handlers.ApplicationHandler(createCustomer)

func createCustomer(w http.ResponseWriter, r *http.Request) {
	app := context.GetApplicationContext(r)
	customerData := new(CreateCustomerData)

	err := json.NewDecoder(r.Body).Decode(customerData)
	if err != nil {
		log.Error().Err(err).Msg("Error decoding create customer data.")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	log.Info().Msgf("New customer create request, with data: %+v", customerData)

	err = createCustomerService(app, customerData)
	if err != nil {
		writeErrMessage(w, err)
		log.Error().Err(err).Msgf("Error creating customer.")
		return
	}

	log.Info().Msgf("Customer with ID: %+v, created", customerData.ID)
	w.WriteHeader(http.StatusCreated)
}

// UpdateCustomerHandler responsible for handling & validating HTTP request
var UpdateCustomerHandler = handlers.ApplicationHandler(updateCustomer)

func updateCustomer(w http.ResponseWriter, r *http.Request) {
	app := context.GetApplicationContext(r)
	routeParams := httprouter.ParamsFromContext(r.Context())
	customerID := routeParams.ByName("customer_id")

	customerData := new(btcsclient.UpdateCustomerRequest)

	err := json.NewDecoder(r.Body).Decode(customerData)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = updateCustomerService(app, customerData, customerID)
	if err != nil {
		jsErr, _ := json.Marshal(err)
		w.Header().Set("Content-Type", "application/json")
		if _, ok := err.(*repo_utils.DoesNotExistsError); ok {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
		w.Write(jsErr)
		return
	}

	w.WriteHeader(http.StatusAccepted)
}

// ResetMediaUploadStatus reset upload status for given media type to allow re-submission flow
var ResetMediaUploadStatus = handlers.ApplicationHandler(resetMediaUploadStatus)

func resetMediaUploadStatus(w http.ResponseWriter, r *http.Request) {
	app := context.GetApplicationContext(r)
	routeParams := httprouter.ParamsFromContext(r.Context())
	customerID := routeParams.ByName("customer_id")
	mediaType := routeParams.ByName("type")

	err := resetMediaUploadStatusService(app, customerID, mediaType)
	if err != nil {
		writeErrMessage(w, err)
		return
	}

	w.WriteHeader(http.StatusAccepted)
}

// SubmitIDScansHandler responsible for handling & validating HTTP request
var SubmitIDScansHandler = handlers.ApplicationHandler(submitIDScans)

func submitIDScans(w http.ResponseWriter, r *http.Request) {
	routeParams := httprouter.ParamsFromContext(r.Context())
	customerID := routeParams.ByName("customer_id")
	app := context.GetApplicationContext(r)
	// TODO: Think about size of the Form
	err := r.ParseMultipartForm(100000)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	m := r.MultipartForm

	err = submitCustomerIDScansService(app, m, customerID)
	if err != nil {
		writeErrMessage(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func writeErrMessage(w http.ResponseWriter, err error)  {
	jsErr, _ := json.Marshal(err)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	w.Write(jsErr)
}

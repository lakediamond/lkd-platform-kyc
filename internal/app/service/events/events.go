package events

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"strconv"

	customer_service "kyc-service/internal/app/service/customer"
	"kyc-service/internal/pkg/application"
	"kyc-service/internal/pkg/repo/models"
	"kyc-service/pkg/btcsclient"
)

// UpdateFileFlagBasedOnEvent parses event and set curresponding FileUploaded flag
func UpdateFileFlagBasedOnEvent(app *application.Application, event *GcpImageMessage) error { // event *S3EventNotification
	customer, err := FindCustomerEventBelongsTo(app, event)
	if err != nil {
		return err
	}
	err = SetCustomerFileUploadedFalgBasedOnEvent(app, customer, event)
	if err != nil {
		return err
	}
	if app.Repo.Customer.DoesAllImageFlagsAreSet(customer) {
		go customer_service.SubmitCustomerIDScansFromS3Service(app, customer.ID)
		go app.LKDBackClient.NotifyBTCSFlowStarted(customer.ID)
	}

	return err
}

// FindCustomerEventBelongsTo function pars GC Storage events Name value to find existing customer in DB based on file path
func FindCustomerEventBelongsTo(app *application.Application, event *GcpImageMessage) (*models.Customer, error) {
	customerID, err := event.GetStorageCustomerID()
	if err != nil {
		log.Error().Err(err).Msgf("getStorageCustomerID: %+v", customerID)
		return nil, err
	}

	customer, err := app.Repo.Customer.GetCustomerByID(customerID)
	if err != nil {
		log.Error().Err(err).Msgf("Parsing event error when setting finding correspondet user. customerID: %+v", customerID)
		return nil, err
	}
	return customer, nil
}

// SetCustomerFileUploadedFalgBasedOnEvent function parses S3 events Key's file name to set curresponding flag
// S3EventNotification
func SetCustomerFileUploadedFalgBasedOnEvent(app *application.Application, customer *models.Customer, event *GcpImageMessage) error {
	uploadedFile, errFile := event.GetStorageUploadedFileName()
	if errFile != nil {
		log.Error().Err(errFile).Msgf("getStorageUploadedFileFile error")
		return errFile
	}

	fileName, fileFormat, _ := ParseStorageFileName(uploadedFile)
	log.Info().Msgf("fileName: %+s, fileFormat: %+s", fileName, fileFormat)

	var err error
	// TODO: refactoring required (atomic transactions, functions abstraction)
	switch fileName {
	case "face_image":
		err = checkFaceImageValidity(app, customer, fileFormat)
		if err != nil {
			log.Error().Err(err).Msgf("checkFaceImageValidity error")
		}
	case "back_image":
		err = checkBackImageValidity(app, customer, fileFormat)
		if err != nil {
			log.Error().Err(err).Msgf("checkFaceImageValidity error")
		}
	case "front_image":
		err = checkFrontImageValidity(app, customer, fileFormat)
		if err != nil {
			log.Error().Err(err).Msgf("checkFrontImageValidity error")
		}

	default:
		err = fmt.Errorf("can not validate image name. FileName: %+s", uploadedFile)
	}
	if err != nil {
		log.Error().Err(err).Msgf("Parsing BTCS event error when setting image flag. Event: %+v", event)
		return err
	}
	return nil
}

// FindCustomerBtcsScanEventBelongsTo function pars BTCS scans events MerchantIDScanReference value to find existing customer in DB based on file path
func FindCustomerBtcsScanEventBelongsTo(app *application.Application, event *BtcsScansEvent) (*models.Customer, error) {
	customerID := strconv.FormatUint(event.CustomerID, 10)
	customer, err := app.Repo.Customer.GetCustomerByKycID(customerID)
	if err != nil {
		log.Error().Msgf("Parsing BTCS event error when finding correspponding user. Error: %+v. Event: %+v", err, event)
		return nil, err
	}
	return customer, nil
}

// ParseBtcsScansEvent parses BtcsScansEvent and sets correspinding flags for corresponding customer
func ParseBtcsScansEvent(app *application.Application, event *BtcsScansEvent) error {
	customer, err := FindCustomerBtcsScanEventBelongsTo(app, event)
	if err != nil {
		log.Error().Msgf("Parsing BTCS event Error: %+v. Event: %+v", err, event)
		return err
	}

	log.Info().Msgf("GORUTINE TO NOTIFY LKD BACK STARTING OK for customer ID %v", customer.ID)
	errorChan := make(chan error)
	go func(app *application.Application, customer *models.Customer) {
		err := NotifyLKDBackednAboutIDVereficationEvent(app, customer)
		if err != nil {
			errorChan <- err
			log.Error().Err(err).Msgf("NotifyLKDBackednAboutIDVereficationEvent error")
			return
		}

		err = app.Repo.Customer.SetCustomerIsWebHook(models.WebHookDone, customer)
		if err != nil {
			errorChan <- err
			log.Error().Err(err).Msgf("Can not set status webHook")
			return
		}

		errorChan <- nil
	}(app, customer)

	checkErr := <-errorChan
	if checkErr != nil {
		log.Error().Err(err).Msgf("GORUTINE NotifyLKDBackednAboutIDVereficationEvent error")
		return checkErr
	}

	return nil
}

// NotifyLKDBackednAboutIDVereficationEvent fetch verefication id traces and push latest to LKD backend
func NotifyLKDBackednAboutIDVereficationEvent(app *application.Application, customer *models.Customer) error {
	resp, err := app.BTCSClient.GetIDVerificationTraceLogs(customer.KycID)
	if err != nil {
		log.Error().Err(err).Msgf("Notify LKD back error fetching ID verefication logs")
		return err
	}

	if resp.Count == 0 {
		log.Error().Msgf("GetIDVerificationTraceLogs error empty.")
		return fmt.Errorf("can not find Verification Trace customer KycID: %+s", customer.KycID)
	}

	status := resp.IDVerifications[0].Status
	lastVereficationResult := resp.IDVerifications[0].Result
	if len(lastVereficationResult.Duplicates) > 0 {
		log.Info().Msgf("Duplicates nb - %d", len(lastVereficationResult.Duplicates))
		resp, err = app.BTCSClient.GetIDVerificationTraceLogs(strconv.FormatUint(lastVereficationResult.Duplicates[0], 10))
		if err != nil {
			log.Error().Err(err).Msgf("Notify LKD back error fetching ID verefication logs")
			return err
		}

		status = resp.IDVerifications[0].Status
		lastVereficationResult = resp.IDVerifications[0].Result
	}

	if status == "WAIT" {
		log.Info().Msgf("VEREFICATION RESULTS IN STATUS WAIT: %+s", customer.KycID)
		return fmt.Errorf("verification scans have status WAIT")
	}

	err = InsertSanctionsMatchesInToIDVereficationResults(app, customer.KycID, &lastVereficationResult)
	if err != nil {
		log.Error().Err(err).Msgf("Notify LKD back error when inserting sanctions math to request")
		return err
	}

	err = app.Repo.Customer.SetScansCheck(status, customer)
	if err != nil {
		log.Error().Err(err).Msgf("Parsing BTCS event error when set DB flags.")
		return err
	}

	log.Info().Msgf("recived traces and submits for customer %v", customer.ID)
	err = app.LKDBackClient.SubmitIDVerificationResults(&lastVereficationResult, customer.ID)
	if err != nil {
		log.Error().Err(err).Msgf("BackClient SubmitIDVerificationResults errors")
		return err
	}

	if err := app.Repo.Customer.SetScansCheckResultSubmitedAt(customer); err != nil {
		log.Error().Err(err).Msgf("Can not set scans check result submited_at")
		return err
	}

	log.Info().Msgf("VEREFICATION RESULTS SENT: %+v", lastVereficationResult)

	return nil
}

// InsertSanctionsMatchesInToIDVereficationResults fetches Customer traces from btcs, extract sanctions match results and inserts in to ID verefication results
func InsertSanctionsMatchesInToIDVereficationResults(app *application.Application, customerID string, results *btcsclient.BtcsScansEventIDVerificationsResult) error {
	traces, err := app.BTCSClient.GetCustomerTaces(customerID)
	if err != nil {
		return err
	}

	matches := btcsclient.FindSanctionsLogsInCustomerTraces(traces)
	results.VerifiedNameSanctionsScreen.Matches = matches
	return nil
}

// GetDuplicateDetails fetches Customer traces from btcs, extract duplicate customer matches, inserts them to ID verification results
func GetDuplicateDetails(app *application.Application, customerID string, results *btcsclient.BtcsScansEventIDVerificationsResult) error {
	traces, err := app.BTCSClient.GetCustomerTaces(customerID)
	if err != nil {
		return err
	}
	duplicates := btcsclient.FindDuplicateDetailsInCustomerTraces(traces)
	results.Duplicates = duplicates
	return nil
}

func checkFaceImageValidity(app *application.Application, customer *models.Customer, fileFormat string) error {
	log.Print("finding face_image")

	keyFace := fmt.Sprintf("%v/identity-scans/%v.%v", customer.ID, "face_image", fileFormat)
	faceImageReader, err := app.Storage.GetImageReader(customer.ID, keyFace)
	if err != nil {
		return err
	}

	imageProcessor := &btcsclient.MultiPartImagesProcessor{
		FaceImage:  faceImageReader,
		BtcsClient: app.BTCSClient,
	}

	err = imageProcessor.CheckFaceImageValidity()
	if err != nil {
		_, ok := err.(*btcsclient.ImageProcessorError)
		if ok {
			app.LKDBackClient.SubmitIDProcessingError(models.FaceImage, customer.ID)
		}
		return err
	}

	_ = app.Repo.Customer.SetFaceImageFlag(true, customer)
	_ = app.Repo.Customer.SetFaceFormat(fileFormat, customer)

	err = app.LKDBackClient.SubmitSuccessImageValidation(customer.ID, "face_image")
	if err != nil {
		return err
	}

	return nil
}

func checkFrontImageValidity(app *application.Application, customer *models.Customer, fileFormat string) error {
	log.Print("finding front_image")

	keyFront := fmt.Sprintf("%v/identity-scans/%v.%v", customer.ID, "front_image", fileFormat)
	frontImageReader, err := app.Storage.GetImageReader(customer.ID, keyFront)
	if err != nil {
		return err
	}

	imageProcessor := &btcsclient.MultiPartImagesProcessor{
		FrontImage: frontImageReader,
		BtcsClient: app.BTCSClient,
	}

	err = imageProcessor.CheckFrontImageValidity()
	if err != nil {
		_, ok := err.(*btcsclient.ImageProcessorError)
		if ok {
			app.LKDBackClient.SubmitIDProcessingError(models.FrontImage, customer.ID)
		}
		return err
	}

	_ = app.Repo.Customer.SetFrontImageFlag(true, customer)
	_ = app.Repo.Customer.SetFrontFormat(fileFormat, customer)

	err = app.LKDBackClient.SubmitSuccessImageValidation(customer.ID, "front_image")
	if err != nil {
		return err
	}

	return nil
}

func checkBackImageValidity(app *application.Application, customer *models.Customer, fileFormat string) error {
	log.Print("finding back_image")

	keyBack := fmt.Sprintf("%v/identity-scans/%v.%v", customer.ID, "back_image", fileFormat)
	backImageReader, err := app.Storage.GetImageReader(customer.ID, keyBack)
	if err != nil {
		return err
	}

	imageProcessor := &btcsclient.MultiPartImagesProcessor{
		BackImage:  backImageReader,
		BtcsClient: app.BTCSClient,
	}

	err = imageProcessor.CheckBackImageValidity()
	if err != nil {
		_, ok := err.(*btcsclient.ImageProcessorError)
		if ok {
			app.LKDBackClient.SubmitIDProcessingError(models.BackImage, customer.ID)
		}
		return err
	}

	_ = app.Repo.Customer.SetBackImageFlag(true, customer)
	_ = app.Repo.Customer.SetBackFormat(fileFormat, customer)

	err = app.LKDBackClient.SubmitSuccessImageValidation(customer.ID, "back_image")
	if err != nil {
		return err
	}

	return nil
}

package btcsclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

type client struct {
	BaseURL          *url.URL
	OrganizationName string
	HTTPClient       *http.Client
	AuthClient       AuthClient
}

// NewBTCSClient instantiates a new `BTCSClient` from the provided baseURL, organization, api key ad secret
func NewBTCSClient(baseURL string, org string, apiKey string, apiSecret string) BTCSClient {

	httpClient := http.Client{Timeout: time.Second * 20}

	url, _ := url.Parse(baseURL)

	return &client{BaseURL: url, OrganizationName: org, HTTPClient: &httpClient,
		AuthClient: AuthClient{APIKey: apiKey, APISecret: apiSecret}}
}

// GetCustomer is the method that returns customer information by its id
func (c *client) GetCustomer(customerId string) (*GetCustomerResponse, error) {
	uri := fmt.Sprintf("/api/v1/customers/%v", customerId)

	req, err := c.makeRequest(http.MethodGet, uri, nil, nil)
	if err != nil {
		return nil, err
	}

	customer := &GetCustomerResponse{}

	_, err = c.do(req, customer)
	if err != nil {
		return nil, err
	}

	return customer, nil
}

func (c *client) GetCustomerLevels(customerID string) (*CustomerLevels, error) {
	uri := fmt.Sprintf("/api/v1/customers/%v/get_levels", customerID)

	req, err := c.makeRequest(http.MethodGet, uri, nil, nil)
	if err != nil {
		return nil, err
	}

	customerLevels := &CustomerLevels{}

	_, err = c.do(req, customerLevels)
	if err != nil {
		return nil, err
	}

	return customerLevels, nil
}

func (c *client) GetCustomerLimits(customerID string) (*CustomerLimits, error) {
	uri := fmt.Sprintf("/api/v1/customers/%v/limits/%v", customerID, c.OrganizationName)

	req, err := c.makeRequest(http.MethodGet, uri, nil, nil)
	if err != nil {
		return nil, err
	}

	customerLevels := &CustomerLimits{}

	_, err = c.do(req, customerLevels)
	if err != nil {
		return nil, err
	}

	return customerLevels, nil
}

func (c *client) GetCustomerTaces(customerID string) (*CustomerTraces, error) {
	uri := fmt.Sprintf("/api/v1/customers/%v/trace", customerID)

	req, err := c.makeRequest(http.MethodGet, uri, nil, nil)
	if err != nil {
		return nil, err
	}

	traces := &CustomerTraces{}

	_, err = c.do(req, traces)
	if err != nil {
		return nil, err
	}

	for key, trace := range traces.Traces {
		traces.Traces[key].RawData = getCustomerTracesFromDTO(trace.RawData)
	}

	return traces, nil
}

func getCustomerTracesFromDTO(data interface{}) CustomerTraceRawData {
	customer := CustomerTraceRawData{}
	switch data.(type) {
	case map[string]interface{}:
		m := data.(map[string]interface{})
		if duplicates, ok := m["duplicates"].([]uint64); ok {
			customer.Duplicates = duplicates
		}
		if matches, ok := m["matches"].([]interface{}); ok {
			customer.Matches = matches
		}
	}

	return customer
}

func (c *client) CreateCustomer(body *CreateCustomerRequest) (*GetCustomerResponse, error) {

	body.Organizations = []string{c.OrganizationName}

	req, err := c.makeRequest(http.MethodPost, "/api/v1/customers/create", body, nil)
	if err != nil {
		return nil, err
	}

	createdCustomer := &GetCustomerResponse{}

	_, err = c.do(req, createdCustomer)

	if err != nil {
		return nil, err
	}

	return createdCustomer, err
}

func (c *client) UpdateCustomer(body *UpdateCustomerRequest, customerID string) (*GetCustomerResponse, error) {
	uri := fmt.Sprintf("/api/v1/customers/%v/update", customerID)

	req, err := c.makeRequest(http.MethodPost, uri, body, nil)
	if err != nil {
		return nil, err
	}

	createdCustomer := &GetCustomerResponse{}

	_, err = c.do(req, createdCustomer)
	if err != nil {
		return nil, err
	}

	return createdCustomer, err
}

func (c *client) SubmitAddress(body *SubmitAddress, customerID string) error {
	uri := fmt.Sprintf("/api/v1/customers/%v/validate_bitcoin_address", customerID)

	req, err := c.makeRequest(http.MethodPost, uri, body, nil)
	if err != nil {
		return err
	}

	resp := make(map[string]interface{})

	_, err = c.do(req, &resp)
	if err != nil {
		return err
	}

	return nil

}

func (c *client) RequestIDScansAccess(customerID string) (*ScanAccessResponse, error) {
	uri := fmt.Sprintf("/api/v1/customers/%v/scans/request_access", customerID)

	req, err := c.makeRequest(http.MethodGet, uri, nil, nil)
	if err != nil {
		return nil, err
	}

	accessResponse := &ScanAccessResponse{}

	_, err = c.do(req, accessResponse)
	if err != nil {
		return nil, err
	}

	return accessResponse, nil
}

func (c *client) UploadIDScan(url string, image io.Reader) error {
	req, err := http.NewRequest("PUT", url, image)
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "image/jpeg")

	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if !(resp.StatusCode >= 200 && resp.StatusCode <= 299) {
		badResponseError := &BtcsApiError{Code: resp.StatusCode}
		err = json.NewDecoder(resp.Body).Decode(badResponseError)
		return badResponseError
	}

	return nil
}

func (c *client) SubmitIDScans(body *UploadIDScansRequest, customerID string) (*GetCustomerResponse, error) {
	uri := fmt.Sprintf("/api/v1/customers/%v/scans/submit", customerID)

	req, err := c.makeRequest(http.MethodPost, uri, body, nil)
	if err != nil {
		return nil, err
	}

	createdCustomer := new(GetCustomerResponse)

	_, err = c.do(req, createdCustomer)
	if err != nil {
		return nil, err
	}

	return createdCustomer, nil
}

func (c *client) GetOrganizationLevels() (*OrganizationLevels, error) {
	uri := fmt.Sprintf("/api/v1/organizations/%v/levels", c.OrganizationName)

	req, err := c.makeRequest(http.MethodGet, uri, nil, nil)
	if err != nil {
		return nil, err
	}

	orgLevels := new(OrganizationLevels)

	_, err = c.do(req, orgLevels)
	if err != nil {
		return nil, err
	}

	return orgLevels, nil
}

func (c *client) GetIDVerificationTraceLogs(customerID string) (*BTCSIDVerificationEventsResponse, error) {
	uri := "/api/v1/kyc/get_id_verifications"
	query := make(map[string]string)
	query["customer_id"] = customerID

	req, err := c.makeRequest(http.MethodGet, uri, nil, query)
	if err != nil {
		return nil, err
	}

	response := new(BTCSIDVerificationEventsResponse)

	_, err = c.do(req, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (c *client) FindCustomer(query map[string]string) (*GetCustomersResponse, error) {
	req, err := c.makeRequest(http.MethodGet, "/api/v1/customers", nil, query)
	if err != nil {
		return nil, err
	}

	findCustomers := &GetCustomersResponse{}

	_, err = c.do(req, findCustomers)

	if err != nil {
		return nil, err
	}

	return findCustomers, err
}

func (c *client) VerifyPhoneSms(customerID string) error {
	uri := fmt.Sprintf("/api/v1/customers/%v/update", customerID)
	body := make(map[string]interface{})
	innerBody := make(map[string]bool)
	innerBody["verified_phone_sms"] = true
	body["verifications"] = innerBody

	req, err := c.makeRequest(http.MethodPost, uri, body, nil)
	if err != nil {
		return err
	}

	createdCustomer := new(GetCustomerResponse)

	_, err = c.do(req, createdCustomer)
	if err != nil {
		return err
	}

	return nil
}

func (c *client) makeRequest(method, path string, body interface{}, query map[string]string) (*http.Request, error) {
	uri := &url.URL{Path: path}
	u := c.BaseURL.ResolveReference(uri)
	q := u.Query()

	if query != nil {
		for k, v := range query {
			q.Set(k, v)
		}
	}

	c.BaseURL.Path = path
	u.RawQuery = q.Encode()

	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return nil, err
		}
	}

	log.Info().Msgf("Making request to BTCS client. Path: %v", u.String())

	req, err := http.NewRequest(method, u.String(), buf)
	if err != nil {
		return nil, err
	}

	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}

	err = c.AuthClient.putAuthHeaders(req)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Accept", "application/json")

	return req, nil
}

func (c *client) do(req *http.Request, target interface{}) (*http.Response, error) {
	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if !(resp.StatusCode >= 200 && resp.StatusCode <= 299) {
		badResponseError := &BtcsApiError{Code: resp.StatusCode}
		err = json.NewDecoder(resp.Body).Decode(badResponseError)
		return resp, badResponseError
	}

	err = json.NewDecoder(resp.Body).Decode(target)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// FindSanctionsLogsInCustomerTraces returns SANCTIONS logs mathces
func FindSanctionsLogsInCustomerTraces(traces *CustomerTraces) []interface{} {
	for _, i := range traces.Traces {
		if strings.Contains(i.Remarks, "VERIFIED_NAME_SANCTIONS_SCREEN") {
			rawData := i.RawData.(CustomerTraceRawData)
			return rawData.Matches
		}
	}
	return nil
}

// FindSanctionsLogsInCustomerTraces returns SANCTIONS logs mathces
func FindDuplicateDetailsInCustomerTraces(traces *CustomerTraces) []uint64 {
	for _, i := range traces.Traces {
		for _, tag := range i.Tag {
			if tag == "KYCTest9" {
				rawData := i.RawData.(CustomerTraceRawData)
				return rawData.Duplicates
			}
		}
	}
	return nil
}

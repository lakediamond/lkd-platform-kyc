package migrations

import (
	"kyc-service/internal/pkg/repo/models"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddCheckResultsFields = gormigrate.Migration{
	ID: "f9918f8d-959f-457f-b7ec-59ebd14b7edb",
	Migrate: func(tx *gorm.DB) error {
		error := tx.AutoMigrate(&models.Customer{}).Error
		if error != nil {
			return error
		}
		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE customers 
		DROP COLUMN scans_check_result_received, 
		DROP COLUMN scans_check_result_received_at, 
		DROP COLUMN scans_check_result, 
		DROP COLUMN address_check_result_received,
		DROP COLUMN address_check_result,
		DROP COLUMN address_check_result_received_at;`
		return tx.Exec(req).Error
	},
}

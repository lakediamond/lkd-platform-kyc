package btcsclient

import (
	"encoding/json"
	"fmt"
)

type BtcsApiError struct {
	Code         int    `json:"message"`
	ErrorMessage string `json:"error"`
}

func (e *BtcsApiError) Error() string {
	return fmt.Sprintf("Bad Response! Code:%+v. Message:%+v", e.Code, e.ErrorMessage)
}

func (e BtcsApiError) MarshalJSON() ([]byte, error) {
	errors := make(map[string]string)
	errors["Error"] = "BadResponse"
	errors["Message"] = e.Error()
	return json.Marshal(errors)
}
